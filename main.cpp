#include <memory>
#include "engine/gfx/glwindow.h"

class Game
{
    GLWindow window {800, 600};

public:
    bool init()
    {
        if (!window.initGL())
            return false;
        window.initEngine();
        window.run();
        return true;
    }
};

int main(void)
{
    Game *game = new Game();
    int initialized = game->init();
    delete game;

    if (!initialized)
        return -1;

    return 0;
}
