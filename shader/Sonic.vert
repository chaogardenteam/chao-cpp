#version 330

const int MAX_BONES = 110;

uniform mat4 projMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform mat4 bones[MAX_BONES];

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;
layout(location = 3) in ivec4 boneIDs;
layout(location = 4) in vec4 weights;

out vec4 vertexPos;
out vec2 TexCoord;
out vec3 Normal;

void main()
{
    mat4 boneTransform = bones[boneIDs[0]] * weights[0];
    boneTransform += bones[boneIDs[1]] * weights[1];
    boneTransform += bones[boneIDs[2]] * weights[2];
    boneTransform += bones[boneIDs[3]] * weights[3];
    
    Normal = normalize(vec3(viewMatrix * modelMatrix * boneTransform * vec4(normal,0.0)));
    TexCoord = vec2(texCoord);
    gl_Position = projMatrix * viewMatrix * modelMatrix * boneTransform * vec4(position,1.0);
}

