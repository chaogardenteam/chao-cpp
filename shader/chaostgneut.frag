#version 330

uniform sampler2D diffuseMap;

in vec3 Normal;
in vec2 TexCoord;
out vec4 outputF;

void main()
{
    vec4 color;
    vec4 amb;
    float intensity;
    vec3 lightDir;
    vec3 n;

    lightDir = normalize(vec3(1.0, 1.0, 1.0));
    n = normalize(Normal);
    intensity = max(dot(lightDir,n),0.0);

    color = texture(diffuseMap, TexCoord);
    amb = color * 0.33;
    outputF = (color * intensity) + amb;
    outputF.a = color.a;
}

