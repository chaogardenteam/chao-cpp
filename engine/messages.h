#ifndef MESSAGES_H
#define MESSAGES_H


class Messages
{
public:
    static Messages* getInstance();
private:
    Messages();
    ~Messages();
};

#endif // MESSAGES_H
