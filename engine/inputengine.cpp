#include "inputengine.h"
#include <GLFW/glfw3.h>
#include <cstring>
#include <iostream>

InputEngine::InputEngine() {
    memset(&keysHeld[0], 0, keysHeld.size());
    memset(&keysPressed[0], 0, keysPressed.size());
}

void InputEngine::keyPressed(int glfwKey) {
    Keys key = keyFromGLFW(glfwKey);
    if (key == KEY_NONE) return;

    if (!keysHeld[key]) {
        keysPressed[key] = true;
        keysHeld[key] = true;
    }
}
void InputEngine::keyReleased(int glfwKey) {
    Keys key = keyFromGLFW(glfwKey);
    if (key == KEY_NONE) return;

    keysHeld[key] = false;
}
void InputEngine::resetPressed() {
    memset(&keysPressed[0], 0, keysPressed.size());
}

bool InputEngine::isKeyPressed(Keys key) {
    return keysPressed[key];
}
bool InputEngine::isKeyHeld(Keys key) {
    return keysHeld[key];
}

Keys InputEngine::keyFromGLFW(int key) {
    switch(key) {
        case GLFW_KEY_UP:
            return KEY_UP;
        case GLFW_KEY_DOWN:
            return KEY_DOWN;
        case GLFW_KEY_LEFT:
            return KEY_LEFT;
        case GLFW_KEY_RIGHT:
            return KEY_RIGHT;
        case GLFW_KEY_SPACE:
            return KEY_JUMP;
        case GLFW_KEY_A:
            return KEY_JUMP;
        case GLFW_KEY_S:
            return KEY_ACTION;
        case GLFW_KEY_Q:
            return KEY_CAMERA_LEFT;
        case GLFW_KEY_W:
            return KEY_CAMERA_RIGHT;
        default:
            return KEY_NONE;
    }
}
