#ifndef PHYSICSSHAPE_H
#define PHYSICSSHAPE_H
#include <memory>
#include <BulletCollision/CollisionShapes/btCollisionShape.h>

class CollisionShape
{
public:
    CollisionShape();

    std::unique_ptr<btCollisionShape> shape;
};

#endif // PHYSICSSHAPE_H
