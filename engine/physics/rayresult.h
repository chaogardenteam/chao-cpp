#ifndef CLOSESTRAYRESULTCALLBACK_H
#define CLOSESTRAYRESULTCALLBACK_H
#include <bullet/BulletCollision/CollisionDispatch/btCollisionWorld.h>
#include <glm/glm.hpp>
#include <glm/vec3.hpp>

class ClosestRayResult : public btCollisionWorld::ClosestRayResultCallback
{
public:
    ClosestRayResult(glm::vec3& rayFromWorld, glm::vec3& rayToWorld);
    glm::vec3 getHitPoint();
    glm::vec3 getHitNormal();
};

#endif // CLOSESTRAYRESULTCALLBACK_H
