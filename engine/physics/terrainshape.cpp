#include "terrainshape.h"
#include "engine/gfx/glmesh.h"

TerrainShape::TerrainShape() :
    indexVertexArray(new btTriangleIndexVertexArray())
{

}

void TerrainShape::addModel(std::shared_ptr<GLMesh> mesh)
{
    btIndexedMesh indexedMesh;
    indexedMesh.m_numTriangles = mesh->numFaces;
    indexedMesh.m_triangleIndexBase = reinterpret_cast<unsigned char*>(&mesh->facesData[0]);
    indexedMesh.m_triangleIndexStride = 3 * sizeof(int);
    indexedMesh.m_numVertices = mesh->numVertices;
    indexedMesh.m_vertexBase = reinterpret_cast<unsigned char*>(&mesh->vertexElementsData[0]);
    indexedMesh.m_vertexStride = mesh->vertexStride;
    indexedMesh.m_vertexType = PHY_FLOAT;

    indexVertexArray->addIndexedMesh(indexedMesh);
    shape.reset(new btBvhTriangleMeshShape(indexVertexArray.get(), true));
    // Keep reference to original model so it doesn't get deleted before this object
    meshes.push_back(mesh);
}
