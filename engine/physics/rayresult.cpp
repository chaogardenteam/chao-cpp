#include "rayresult.h"

inline btVector3 glmVec3ToBullet(glm::vec3& vector) {
    return btVector3(vector.x, vector.y, vector.z);
}

ClosestRayResult::ClosestRayResult(glm::vec3 &rayFromWorld, glm::vec3 &rayToWorld) :
    ClosestRayResultCallback(glmVec3ToBullet(rayFromWorld), glmVec3ToBullet(rayToWorld)) { }

glm::vec3 ClosestRayResult::getHitPoint()
{
    return glm::vec3(m_hitPointWorld.x(), m_hitPointWorld.y(), m_hitPointWorld.z());
}

glm::vec3 ClosestRayResult::getHitNormal()
{
    return glm::vec3(m_hitNormalWorld.x(), m_hitNormalWorld.y(), m_hitNormalWorld.z());
}
