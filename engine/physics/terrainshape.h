#ifndef TERRAINSHAPE_H
#define TERRAINSHAPE_H
#include "collisionshape.h"
#include <memory>
#include <vector>
#include <BulletCollision/CollisionShapes/btBvhTriangleMeshShape.h>
#include <BulletCollision/CollisionShapes/btTriangleIndexVertexArray.h>

class GLMesh;

class TerrainShape : public CollisionShape
{
private:
    std::vector<std::shared_ptr<GLMesh>> meshes;
    std::unique_ptr<btTriangleIndexVertexArray> indexVertexArray;
public:    
    TerrainShape();
    void addModel(std::shared_ptr<GLMesh> mesh);
};

#endif // TERRAINSHAPE_H
