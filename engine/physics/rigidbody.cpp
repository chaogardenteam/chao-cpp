#include "rigidbody.h"
#include <bullet/LinearMath/btDefaultMotionState.h>
#include <bullet/BulletDynamics/Dynamics/btRigidBody.h>

RigidBody::RigidBody(std::shared_ptr<CollisionShape> shape, float mass)
{
    btTransform startTransform;
    startTransform.setIdentity();

    motionState.reset(new btDefaultMotionState(startTransform));
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState.get(), shape->shape.get());
    body.reset(new btRigidBody(rbInfo));
}

