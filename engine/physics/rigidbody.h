#ifndef RIGIDBODY_H
#define RIGIDBODY_H
#include "collisionshape.h"

class btDefaultMotionState;
class btRigidBody;

class RigidBody
{
public:
    RigidBody(std::shared_ptr<CollisionShape> shape, float mass);

    std::unique_ptr<btDefaultMotionState> motionState;
    std::unique_ptr<btRigidBody> body;
};

#endif // RIGIDBODY_H
