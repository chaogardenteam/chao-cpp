#ifndef PHYSICSENGINE_H
#define PHYSICSENGINE_H
#include <btBulletDynamicsCommon.h>
#include <glm/vec3.hpp>
#include "engine/physics/rigidbody.h"
#include "engine/physics/rayresult.h"


class PhysicsEngine
{
private:
    btDefaultCollisionConfiguration* collisionConfiguration;
    btCollisionDispatcher* dispatcher;
    btBroadphaseInterface* overlappingPairCache;
    btSequentialImpulseConstraintSolver* solver;
    btDiscreteDynamicsWorld* dynamicsWorld;
public:
    PhysicsEngine();
    ~PhysicsEngine();
    void initPhysics();
    void addRigidBody(RigidBody* rigidBody);
    void rayTest(glm::vec3 &rayFromWorld, glm::vec3 &rayToWorld, btCollisionWorld::RayResultCallback &resultCallback);
    void serialize();
};

#endif // PHYSICSENGINE_H
