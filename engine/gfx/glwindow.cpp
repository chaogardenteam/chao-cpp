#include "glwindow.h"
#include <iostream>
#include <thread>

GLWindow::GLWindow(int width, int height)
        : width(width), height(height)
    { }

bool GLWindow::initGL()
{
    /* Initialize the library */
    if (!glfwInit())
        return false;

    /* Request Specific Version */
#ifdef USE_OPENGL_ES
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
#else
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(width, height, "Chao Garden", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return false;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);

    /* Disable vsync */
    glfwSwapInterval(0);

    return true;
}

void GLWindow::initEngine()
{
    engine = Engine::getInstance();
    engine->init();
    engine->graphics->setup_context(800, 600);

    glfwSetKeyCallback(window, keyCallback);
}

void GLWindow::run()
{
    lastTime = glfwGetTime();

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        /* Calculate time passed since previous frame */
        double currentTime = glfwGetTime();
        float deltaTime = (float)(currentTime - lastTime);
        lastTime = currentTime;

        /* Measure speed */
        framesCount++;
        if ( currentTime - frameCounterResetTime >= 1.0 )
        {
            std::cout << framesCount << " frames/second, " <<
                         (1000.0/double(framesCount)) << " ms/frame\n";
            framesCount = 0;
            frameCounterResetTime += 1.0;
        }

        /* Update world */
        std::thread updateThread(&Engine::update, engine, deltaTime);

        /* Render here */
        engine->render();

        /* Join back update thread */
        updateThread.join();

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glfwTerminate();
}

void GLWindow::keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    auto engine = Engine::getInstance();
    if (action == GLFW_PRESS)
        engine->input->keyPressed(key);
    else if (action == GLFW_RELEASE)
        engine->input->keyReleased(key);
}

GLWindow::~GLWindow()
{
}
