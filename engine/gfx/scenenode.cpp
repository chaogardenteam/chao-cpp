#include "scenenode.h"

SceneNode::SceneNode(std::shared_ptr<GLMesh> model):
    renderParams(new RenderParams()),
    updatedRenderParams(new RenderParams())
{
    this->model = model;
}

void SceneNode::setRenderParams(RenderParams& params)
{
    renderParamsMutex.lock();
    *updatedRenderParams = params;
    renderParamsMutex.unlock();
}

void SceneNode::updateRenderParams()
{
    renderParamsMutex.lock();
    *renderParams = *updatedRenderParams;
    renderParamsMutex.unlock();
}

void SceneNode::render(RenderParams &cameraRenderParams)
{
    updateRenderParams();
    renderParams->matrices["projMatrix"] = cameraRenderParams.matrices["projMatrix"];
    renderParams->matrices["viewMatrix"] = cameraRenderParams.matrices["viewMatrix"];
    model->render(*renderParams);
}
