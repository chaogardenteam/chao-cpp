#include "animationplayer.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

AnimationPlayer::AnimationPlayer(std::shared_ptr<GLMesh> mesh) :
    mesh(mesh)
{
    // Fill in default bone transforms
    boneTransforms.resize(mesh->numBones);
}

AnimationPlayer::~AnimationPlayer()
{

}

void AnimationPlayer::transformBones(Animation &animation, const MeshNode &node, const glm::mat4 &parentTransform)
{
    auto nodeTransformation = node.transformation;
    auto iterator = animation.nodes.find(node.name);
    if (iterator != animation.nodes.end())
    {
        auto &animNode = animation.nodes[node.name];

        if (interpolateAnimations)
        {
            auto position = calcInterpolatedKey(animNode.positionKeys);
            auto scale = calcInterpolatedKey(animNode.scalingKeys);
            auto rotation = calcInterpolatedKey(animNode.rotationKeys);

            nodeTransformation = glm::translate(glm::mat4(), position) *
                glm::toMat4(rotation) *
                glm::scale(glm::mat4(), scale);
        }
        else
        {
            int positionKeyIndex, scalingKeyIndex, rotationKeyIndex;
            if (animNode.keyFramePerTick)
            {
                positionKeyIndex = scalingKeyIndex = rotationKeyIndex = (int)animationTick;
            }
            else
            {
                positionKeyIndex = findKey(animNode.positionKeys);
                scalingKeyIndex = findKey(animNode.scalingKeys);
                rotationKeyIndex = findKey(animNode.rotationKeys);
            }

            auto &position = animNode.positionKeys[positionKeyIndex].value;
            auto &scale = animNode.scalingKeys[scalingKeyIndex].value;
            auto &rotation = animNode.rotationKeys[rotationKeyIndex].value;

            nodeTransformation = glm::translate(glm::mat4(), position) *
                glm::toMat4(rotation) *
                glm::scale(glm::mat4(), scale);
        }
    }
	auto globalTransformation = parentTransform * nodeTransformation;

    auto& boneMapping = mesh->boneMapping;
    if (boneMapping.find(node.name) != boneMapping.end())
    {
        uint boneIndex = boneMapping[node.name];
        boneTransforms[boneIndex] = glm::inverse(mesh->rootNode->transformation) *
                globalTransformation * mesh->bones[boneIndex].offsetMatrix;
    }

    for (int i=0; i < node.numChildren; i++)
    {
        transformBones(animation, node.children[i], globalTransformation);
    }
}

glm::quat AnimationPlayer::calcInterpolatedKey(std::vector<AnimationRotationKey> &keys)
{
    if (keys.size() == 1)
        return keys[0].value;

    uint keyIndex = findKey(keys);
    uint nextKeyIndex = (keyIndex + 1);
    auto& key = keys[keyIndex];
    auto& nextKey = keys[nextKeyIndex];

    int deltaTime = nextKey.time - key.time;
    float factor = (animationTick - (float)key.time) / deltaTime;

    return glm::slerp(key.value, nextKey.value, factor);
}

glm::vec3 AnimationPlayer::calcInterpolatedKey(std::vector<AnimationKey> &keys)
{
    if (keys.size() == 1)
        return keys[0].value;

    uint keyIndex = findKey(keys);
    uint nextKeyIndex = (keyIndex + 1);
    auto& key = keys[keyIndex];
    auto& nextKey = keys[nextKeyIndex];

    int deltaTime = nextKey.time - key.time;
    float factor = (animationTick - (float)key.time) / deltaTime;

    return glm::mix(key.value, nextKey.value, factor);
}

void AnimationPlayer::playAnimation(float deltaTime)
{
    if (mesh->animations.find(animationName) != mesh->animations.end())
    {
        auto& animation = mesh->animations[animationName];
        animationTick += deltaTime * animation.ticksPerSecond;

        while (animationTick > animation.duration)
            animationTick -= animation.duration;

        transformBones(animation, *mesh->rootNode, glm::mat4(1.0f));
    }
}

void AnimationPlayer::startAnimation(std::string animationName)
{
    this->animationName = animationName;
}

template<typename T>
uint AnimationPlayer::findKey(std::vector<T> &keys)
{
    uint numKeys = keys.size() - 1;
    for (uint i = 0; i < numKeys; i++)
    {
        if (animationTick < (float)keys[i + 1].time)
            return i;
    }
    return 0;
}
