#ifndef GLMESH_H
#define GLMESH_H
#include <glm/gtc/quaternion.hpp>
#include <glad/glad.h>
#include <assimp/scene.h>
#include <array>
#include <memory>
#include <string>
#include "glshader.h"
#include "gltexture.h"
#include "renderparams.h"

typedef unsigned int uint;

#define MAX_BONES 110

struct MeshNode
{
    std::string name;
    int numChildren = 0;
    int numMeshes = 0;
    std::vector<MeshNode> children;
    std::vector<uint> meshes;
    glm::mat4 transformation;
};

struct MeshPart
{
    int numFaces = 0;
    int numVertices = 0;
    int indexFaces = 0;
    int indexVertices = 0;
    int indexMaterial = 0;
    int baseVertex = 0;
};

struct BoneInfo
{
    glm::mat4 offsetMatrix;
};

struct BoneData
{
    int boneIDs[4];
    float boneWeights[4];
};

struct AnimationKey
{
    int time = 0;
    glm::vec3 value = glm::vec3();
};

struct AnimationRotationKey
{
    int time = 0;
    glm::quat value = glm::quat();
};

struct AnimationNode
{
    bool keyFramePerTick;
    std::vector<AnimationKey> positionKeys;
    std::vector<AnimationKey> scalingKeys;
    std::vector<AnimationRotationKey> rotationKeys;
    std::vector<glm::mat4> transforms;
};

struct Animation
{
    int duration;
    int ticksPerSecond;
    std::map<std::string, AnimationNode> nodes;
};

struct Material
{
    std::shared_ptr<GLTexture> textureDiffuse;
    std::shared_ptr<GLShader> shader;
};

class GLMesh
{
public:
    GLMesh() {}
    ~GLMesh();
    bool load(const std::string &filename, int flags);
    bool loadAnimation(const std::string &filename, const std::string animationName, int flags = 0);
    void render(RenderParams& renderParams, MeshNode &node);
    void render(RenderParams& renderParams);
    void genVAO();
    void calculateVertexStride();

    std::string filename;
    std::string dirname;

    int numBones = 0;
    std::map<std::string, int> boneMapping;
    std::vector<BoneInfo> bones;
    std::map<std::string, Animation> animations;
    std::unique_ptr<MeshNode> rootNode;

    bool hasNormals = false;
    bool hasBones = false;
    int numVertices = 0;
    int numFaces = 0;
    int numColorChannels = 0;
    int numUVChannels = 0;
    int numMeshParts = 0;
    int numMaterials = 0;

    std::vector<MeshPart> meshParts;
    std::vector<Material> materials;
    std::vector<GLfloat> vertexElementsData;
    std::vector<GLuint> facesData;

    int vertexStride = 3 * sizeof(float);
private:
    GLuint vertexArrayObject = 0;
    GLuint vertexElementsBuffer = 0;
    GLuint facesBuffer = 0;
    int indexVBO = 0;
    int indexIBO = 0;

    std::shared_ptr<GLShader> shader;
    GLuint currentProgramID = -1;

    void copyAiMaterials(const aiScene *scene);
    void copyAiMeshes(const aiScene *scene);
    void copyAiNodes(const aiNode *node, MeshNode *targetNode);
    void copyAiBones(MeshPart *meshPart, const aiMesh *aimesh);
    void copyAiAnimations(const aiScene *scene);
    void copyAiAnimation(const aiAnimation *animation, Animation &target);
    void addBoneDataToVBO(MeshPart &meshPart, uint vertexID, uint boneIndex, float weight);
};

#endif // GLMESH_H
