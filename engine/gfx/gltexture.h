#ifndef GLTEXTURE_H
#define GLTEXTURE_H
#include <string>
#include <glad/glad.h>
#include <IL/il.h>

class GLTexture
{
public:
    GLTexture();
    ~GLTexture();
    bool load(const std::string &filename);
    void bind();
private:
    ILuint ilTextureID;
    GLuint textureID;
};
void DevILInit();
#endif // GLTEXTURE_H
