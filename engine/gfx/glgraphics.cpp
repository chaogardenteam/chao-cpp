#include "glgraphics.h"
#include <glad/glad.h>
#include "gltexture.h"

GLGraphics::GLGraphics()
{
    DevILInit();
}

void GLGraphics::setup_context(int width, int height)
{
    this->width = width;
    this->height = height;
    glEnable(GL_DEPTH_TEST);
    //   glEnable(GL_CULL_FACE);
    //   glCullFace(GL_FRONT);
    glDepthFunc(GL_LESS);
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void GLGraphics::render()
{
    clear();
}

void GLGraphics::clear()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void GLGraphics::clearColor()
{
    glClear(GL_COLOR_BUFFER_BIT);
}

void GLGraphics::clearDepth()
{
    glClear(GL_DEPTH_BUFFER_BIT);
}

float GLGraphics::aspectRatio()
{
    return (float)width / height;
}

GLGraphics::~GLGraphics()
{

}

