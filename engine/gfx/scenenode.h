#ifndef SCENENODE_H
#define SCENENODE_H
#include <memory>
#include <mutex>
#include "renderparams.h"
#include "glmesh.h"

class SceneNode
{
public:
    SceneNode(std::shared_ptr<GLMesh> model = nullptr);
    std::unique_ptr<RenderParams> renderParams;
    void setRenderParams(RenderParams &params);
    void updateRenderParams();
    void render(RenderParams &parentParams);
private:
    std::shared_ptr<GLMesh> model;
    std::unique_ptr<RenderParams> updatedRenderParams;
    std::mutex renderParamsMutex;
};

#endif // SCENENODE_H
