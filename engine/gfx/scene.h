#ifndef SCENE_H
#define SCENE_H
#include <memory>
#include <vector>
#include "scenenode.h"

class Scene
{
public:
    Scene();
    void render();

    std::unique_ptr<SceneNode> cameraNode;
    void add(SceneNode* node);
private:
    std::vector<SceneNode*> nodes;
};

#endif // SCENE_H
