#ifndef GLSHADER_H
#define GLSHADER_H
#include <glad/glad.h>
#include <string>
#include <glm/glm.hpp>
#include "renderparams.h"

class GLShader
{
public:
    GLShader() {}
    ~GLShader();
    bool load(const std::string &filename);
    void compileShader();
    void use();
    GLuint getProgramID();

    void setUniform(std::string name, float *values, int count);
    void setUniform(std::string name, const float value);
    void setUniform(std::string name, glm::vec2 *vectors, int count);
    void setUniform(std::string name, const glm::vec2 &vector);
    void setUniform(std::string name, glm::vec3 *vectors, int count);
    void setUniform(std::string name, const glm::vec3 &vector);
    void setUniform(std::string name, glm::vec4 *vectors, int count);
    void setUniform(std::string name, const glm::vec4 &vector);
    void setUniform(std::string name, glm::mat3 *matrices, int count);
    void setUniform(std::string name, const glm::mat3 &matrix);
    void setUniform(std::string name, glm::mat4 *matrices, int count);
    void setUniform(std::string name, std::vector<glm::mat4> matrices);
    void setUniform(std::string name, const glm::mat4 &matrix);
    void setUniform(std::string name, int *values, int count);
    void setUniform(std::string name, const int value);
	void setModelAndNormalMatrix(std::string modelMatrixName, std::string normalMatrixName, glm::mat4 &modelMatrix);
    void setModelAndNormalMatrix(std::string modelMatrixName, std::string normalMatrixName, glm::mat4 *modelMatrix);

    void setUniforms(RenderParams& renderParams);
private:
    GLuint programID;
    std::string filename;
    std::string vertexShaderCode;
    std::string fragmentShaderCode;
    std::string getCodeFromFile(const std::string &filename);
    void checkShader(int shaderID);
};

#endif // GLSHADER_H
