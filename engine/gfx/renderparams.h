#ifndef RENDERPARAMS_H
#define RENDERPARAMS_H
#include <map>
#include <string>
#include <vector>
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>

struct RenderParams
{
    std::map<std::string, glm::mat4> matrices;
    std::map<std::string, std::vector<glm::mat4>> matrixArrays;
    std::map<std::string, int> samplers;
    std::map<std::string, glm::vec4> vectors;

    RenderParams merge(RenderParams params) {
        params.matrices.insert(matrices.begin(), matrices.end());
        params.matrixArrays.insert(matrixArrays.begin(), matrixArrays.end());
        params.samplers.insert(samplers.begin(), samplers.end());
        params.vectors.insert(vectors.begin(), vectors.end());
        return params;
    }
};

#endif // RENDERPARAMS_H
