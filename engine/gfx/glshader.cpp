#include "glshader.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <memory>

bool GLShader::load(const std::string& filename)
{
    this->filename = filename;
    vertexShaderCode = getCodeFromFile(filename + ".vert");
    if (!vertexShaderCode.size())
    {
        std::cout << "[Shader] Can't load " << filename << ".vert" << std::endl;
    }

    fragmentShaderCode = getCodeFromFile(filename + ".frag");
    if (!fragmentShaderCode.size())
    {
        std::cout << "[Shader] Can't load " << filename << ".frag" << std::endl;
    }
    compileShader();
    return true;
}

std::string GLShader::getCodeFromFile(const std::string& filename)
{
    std::string shaderCode;
    std::ifstream shaderStream(filename, std::ios::in);
    if (shaderStream.is_open())
    {
        std::string line = "";
        while(std::getline(shaderStream, line))
            shaderCode += "\n" + line;
        shaderStream.close();
    }
    return shaderCode;
}

void GLShader::compileShader()
{
    auto vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    auto fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    // Compile Vertex Shader
    char const *vertexSourcePointer = vertexShaderCode.c_str();
    glShaderSource(vertexShaderID, 1, &vertexSourcePointer , NULL);
    glCompileShader(vertexShaderID);
    checkShader(vertexShaderID);
 
    // Compile Fragment Shader
    char const *fragmentSourcePointer = fragmentShaderCode.c_str();
    glShaderSource(fragmentShaderID, 1, &fragmentSourcePointer , NULL);
    glCompileShader(fragmentShaderID);
    checkShader(fragmentShaderID);
 
    // Link the program
    programID = glCreateProgram();
    glAttachShader(programID, vertexShaderID);
    glAttachShader(programID, fragmentShaderID);
    glLinkProgram(programID);

    glDeleteShader(vertexShaderID);
    glDeleteShader(fragmentShaderID);
}

void GLShader::use()
{
    glUseProgram(programID);
}

GLuint GLShader::getProgramID()
{
    return programID;
}

void GLShader::checkShader(int shaderID)
{
    GLint result = GL_FALSE;
    int infoLogLength;

    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
    glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);

    // One byte long log contains just string termination byte
    if (infoLogLength > 1)
	{
        auto errorMessage = std::make_unique<char[]>(infoLogLength);
        glGetShaderInfoLog(shaderID, infoLogLength, NULL, errorMessage.get());
        std::cout << "[Shader] " << filename << " " << errorMessage.get() << std::endl;
	}

}

// Setting floats

void GLShader::setUniform(std::string name, float* values, int count)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniform1fv(loc, count, values);
}

void GLShader::setUniform(std::string name, const float value)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniform1fv(loc, 1, &value);
}

// Setting vectors

void GLShader::setUniform(std::string name, glm::vec2* vectors, int count)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniform2fv(loc, count, (GLfloat*)vectors);
}

void GLShader::setUniform(std::string name, const glm::vec2 &vector)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniform2fv(loc, 1, (GLfloat*)&vector);
}

void GLShader::setUniform(std::string name, glm::vec3* vectors, int count)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniform3fv(loc, count, (GLfloat*)vectors);
}

void GLShader::setUniform(std::string name, const glm::vec3 &vector)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniform3fv(loc, 1, (GLfloat*)&vector);
}

void GLShader::setUniform(std::string name, glm::vec4* vectors, int count)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniform4fv(loc, count, (GLfloat*)vectors);
}

void GLShader::setUniform(std::string name, const glm::vec4 &vector)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniform4fv(loc, 1, (GLfloat*)&vector);
}

// Setting 3x3 matrices

void GLShader::setUniform(std::string name, glm::mat3* matrices, int count)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniformMatrix3fv(loc, count, false, (GLfloat*)matrices);
}

void GLShader::setUniform(std::string name, const glm::mat3 &matrix)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniformMatrix3fv(loc, 1, false, (GLfloat*)&matrix);
}

// Setting 4x4 matrices

void GLShader::setUniform(std::string name, glm::mat4* matrices, int count)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniformMatrix4fv(loc, count, false, (GLfloat*)matrices);
}

void GLShader::setUniform(std::string name, std::vector<glm::mat4> matrices)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniformMatrix4fv(loc, matrices.size(), false, (GLfloat*)matrices.data());
}

void GLShader::setUniform(std::string name, const glm::mat4 &matrix)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniformMatrix4fv(loc, 1, false, (GLfloat*)&matrix);
}

// Setting integers

void GLShader::setUniform(std::string name, int* values, int count)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniform1iv(loc, count, values);
}

void GLShader::setUniform(std::string name, const int value)
{
    int loc = glGetUniformLocation(programID, name.c_str());
    glUniform1i(loc, value);
}

void GLShader::setModelAndNormalMatrix(std::string modelMatrixName, std::string normalMatrixName, glm::mat4 &modelMatrix)
{
    setUniform(modelMatrixName, modelMatrix);
    setUniform(normalMatrixName, glm::transpose(glm::inverse(modelMatrix)));
}

void GLShader::setModelAndNormalMatrix(std::string modelMatrixName, std::string normalMatrixName, glm::mat4* modelMatrix)
{
    setUniform(modelMatrixName, *modelMatrix);
    setUniform(normalMatrixName, glm::transpose(glm::inverse(*modelMatrix)));
}

void GLShader::setUniforms(RenderParams &renderParams)
{
    for (auto& uniform : renderParams.matrices)
    {
        setUniform(uniform.first, uniform.second);
    }
    for (auto& uniform: renderParams.matrixArrays)
    {
        setUniform(uniform.first, uniform.second);
    }
    for (auto& uniform : renderParams.samplers)
    {
        setUniform(uniform.first, uniform.second);
    }
}

GLShader::~GLShader()
{
}
