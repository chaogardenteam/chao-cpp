#ifndef ANIMATIONPLAYER_H
#define ANIMATIONPLAYER_H
#include "glmesh.h"

class AnimationPlayer
{
public:
    AnimationPlayer(std::shared_ptr<GLMesh> mesh);
    ~AnimationPlayer();
    std::vector<glm::mat4> boneTransforms;
    void playAnimation(float deltaTime);
    void startAnimation(std::string animationName);
private:
    bool interpolateAnimations = true;
    void transformBones(Animation &animation, const MeshNode &node, const glm::mat4 &parentTransform);
    template<typename T> uint findKey(std::vector<T> &keys);
    glm::quat calcInterpolatedKey(std::vector<AnimationRotationKey> &keys);
    glm::vec3 calcInterpolatedKey(std::vector<AnimationKey> &keys);

    std::shared_ptr<GLMesh> mesh;
    std::string animationName;
    float animationTick = 0;

};

#endif // ANIMATIONPLAYER_H
