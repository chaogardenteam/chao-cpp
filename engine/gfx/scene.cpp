#include "scene.h"

Scene::Scene() :
    cameraNode(new SceneNode())
{

}

void Scene::render()
{
    cameraNode->updateRenderParams();
    auto &renderParams = cameraNode->renderParams;

    for (auto& node : nodes)
    {
        node->render(*renderParams);
    }
}

void Scene::add(SceneNode* node)
{
    nodes.push_back(node);
}
