#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "../engine.h"

class GLWindow
{
public:
    GLWindow(int width, int height);
    ~GLWindow();
    bool initGL();
    void initEngine();
    void run();
    static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
private:
    GLFWwindow* window;
    Engine* engine = nullptr;
    int width, height;
    double lastTime = 0.0;
    double frameCounterResetTime = 0.0;
    int framesCount = 0;
};
