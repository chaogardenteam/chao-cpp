#include "gltexture.h"
#include <iostream>
#include <IL/il.h>
#include <glad/glad.h>

GLTexture::GLTexture()
{
    ilGenImages(1, &ilTextureID);
}

GLTexture::~GLTexture()
{
    ilDeleteImages(1, &ilTextureID);
    glDeleteTextures(1, &textureID);
}

bool GLTexture::load(const std::string& filename)
{
    ilBindImage(ilTextureID);

    if (!ilLoadImage((const ILstring)filename.c_str()))
    {
        std::cout << "[Textures] Can't load texture: " << filename << std::endl;
        return false;
    }

    int width  = ilGetInteger(IL_IMAGE_WIDTH);
    int height = ilGetInteger(IL_IMAGE_HEIGHT);
    int type   = ilGetInteger(IL_IMAGE_TYPE);
    int fmt    = ilGetInteger(IL_IMAGE_FORMAT);

    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, fmt, width, height, 0,
                 fmt, type, ilGetData());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R,     GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);

    return true;
}

void GLTexture::bind()
{
    glBindTexture(GL_TEXTURE_2D, textureID);
}

void DevILInit()
{
    ilInit();
}
