#ifndef GLGRAPHICS_H
#define GLGRAPHICS_H

class GLGraphics
{
public:
    GLGraphics();
    ~GLGraphics();
    void setup_context(int width, int height);
    void render();
    void clear();
    void clearColor();
    void clearDepth();
    float aspectRatio();

    int width, height;
};

#endif // GLGRAPHICS_H
