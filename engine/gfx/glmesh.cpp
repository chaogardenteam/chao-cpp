#include "glmesh.h"
#include <iostream>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "../resources.h"

bool GLMesh::load(const std::string& filename, int flags)
{
    this->filename = filename;
    dirname = getDirName(filename);

    // Create an instance of the Importer class
    Assimp::Importer importer;

    const aiScene* scene = importer.ReadFile( filename, flags |
          aiProcess_CalcTangentSpace       |
          aiProcess_Triangulate            |
          aiProcess_JoinIdenticalVertices  |
          aiProcess_SortByPType            |
          aiProcess_LimitBoneWeights
    );
    //const aiScene* scene = importer.ReadFile( filename, aiProcessPreset_TargetRealtime_MaxQuality);

    // If the import failed, report it
    if(!scene)
    {
        auto error = importer.GetErrorString();
        std::cout << error;
        return false;
    }

    numVertices = 0;
    numFaces = 0;

    for (uint i = 0; i < scene->mNumMeshes; i++)
    {
        numVertices += scene->mMeshes[i]->mNumVertices;
        numFaces += scene->mMeshes[i]->mNumFaces;
    }
    // FIXME: What if different parts of model have different vertex channels available?
    hasNormals = scene->mMeshes[0]->HasNormals();
    hasBones = scene->mMeshes[0]->HasBones();
    numColorChannels = scene->mMeshes[0]->GetNumColorChannels();
    numUVChannels = scene->mMeshes[0]->GetNumUVChannels();

    // Vertex stride = size of data for each vertice
    calculateVertexStride();

    // Allocate memory for keeping our VBO and IBO data
    vertexElementsData.resize(numVertices * vertexStride / sizeof(float));
    facesData.resize(numFaces * 3);

    // Prepare structures and data for rendering
    copyAiMeshes(scene);
    copyAiMaterials(scene);

    rootNode.reset(new MeshNode());
    copyAiNodes(scene->mRootNode, rootNode.get());
    copyAiAnimations(scene);

    // Upload data to GPU
    genVAO();

    return true;
}

bool GLMesh::loadAnimation(const std::string& filename, const std::string animationName, int flags)
{
    this->filename = filename;
    dirname = getDirName(filename);

    // Create an instance of the Importer class
    Assimp::Importer importer;

    const aiScene* scene = importer.ReadFile( filename, flags |
          aiProcess_CalcTangentSpace       |
          aiProcess_Triangulate            |
          aiProcess_JoinIdenticalVertices  |
          aiProcess_SortByPType            |
          aiProcess_LimitBoneWeights
    );
    //const aiScene* scene = importer.ReadFile( filename, aiProcessPreset_TargetRealtime_MaxQuality);

    // If the import failed, report it
    if(!scene)
    {
        auto error = importer.GetErrorString();
        std::cout << error;
        return false;
    }

    if (scene->mNumAnimations > 0)
    {
        auto aianimation = scene->mAnimations[0];
        copyAiAnimation(aianimation, animations[animationName]);

        return true;
    }

    return false;
}

void GLMesh::copyAiMeshes(const aiScene* scene)
{
    numMeshParts = scene->mNumMeshes;
    meshParts.resize(numMeshParts);

    for (int i = 0; i < numMeshParts; i++)
    {
        auto meshPart = &meshParts[i];
        auto aimesh = scene->mMeshes[i];

        meshPart->indexMaterial = aimesh->mMaterialIndex;
        meshPart->numFaces = aimesh->mNumFaces;
        meshPart->numVertices = aimesh->mNumVertices;
        meshPart->indexVertices = indexVBO;
        meshPart->indexFaces = indexIBO;
        meshPart->baseVertex = (meshPart->indexVertices * sizeof(GLfloat) / (vertexStride));

        for (int n = 0; n < meshPart->numVertices; n++)
        {
            for (int v = 0; v < 3; v++)
                vertexElementsData[indexVBO++] = aimesh->mVertices[n][v];

            if (hasNormals)
                for (int v = 0; v < 3; v++)
                    vertexElementsData[indexVBO++] = aimesh->mNormals[n][v];

            for (int channel = 0; channel < numColorChannels; channel++)
                for (int v = 0; v < 4; v++)
                    vertexElementsData[indexVBO++] = aimesh->mColors[channel][n][v];

            for (int channel = 0; channel < numUVChannels; channel++)
                for (int v = 0; v < 2; v++)
                    vertexElementsData[indexVBO++] = aimesh->mTextureCoords[channel][n][v];

            if (hasBones)
            {
                // Reserve space for bone IDs and weights
                auto& boneDataVBO = reinterpret_cast<BoneData &>(vertexElementsData[indexVBO]);

                // Reserve space for bone IDs
                for (int bone = 0; bone < AI_LMW_MAX_WEIGHTS; bone++)
                {
                    boneDataVBO.boneIDs[bone] = 0;
                    boneDataVBO.boneWeights[bone] = 0.0f;
                }
                indexVBO += sizeof(BoneData) / sizeof(float);
            }
        }

        for (int n = 0; n < meshPart->numFaces; n++)
            for (int v = 0; v < 3; v++)
                facesData[indexIBO++] = aimesh->mFaces[n].mIndices[v] + meshPart->baseVertex;

        if (hasBones)
            copyAiBones(meshPart, aimesh);
    }
}

void GLMesh::copyAiMaterials(const aiScene* scene)
{
    numMaterials = scene->mNumMaterials;
    materials.resize(numMaterials);

    for (int i = 0; i < numMaterials; i++)
    {
        auto& material = materials[i];
        auto aimaterial = scene->mMaterials[i];

        int texIndex = 0;
        aiString path;

        // TODO: implement multiple diffuse textures for texture blending and GIA
        if(aimaterial->GetTexture(aiTextureType_DIFFUSE, texIndex, &path) == AI_SUCCESS)
        {
            material.textureDiffuse = Resources::getTexture(dirname + path.data);
        }
        // TODO: implement material -> shader mapping, probably using some configuration format
        //material.shader = Resources::getShader("shader/default");
        material.shader = Resources::getShader("shader/" + getBaseName(filename));
    }
}

void copyAiMat(const aiMatrix4x4 *from, glm::mat4 &to) {
    to[0][0] = from->a1; to[1][0] = from->a2;
    to[2][0] = from->a3; to[3][0] = from->a4;
    to[0][1] = from->b1; to[1][1] = from->b2;
    to[2][1] = from->b3; to[3][1] = from->b4;
    to[0][2] = from->c1; to[1][2] = from->c2;
    to[2][2] = from->c3; to[3][2] = from->c4;
    to[0][3] = from->d1; to[1][3] = from->d2;
    to[2][3] = from->d3; to[3][3] = from->d4;
}

void GLMesh::copyAiBones(MeshPart *meshPart, const aiMesh* aimesh)
{
    for (uint i = 0; i < aimesh->mNumBones; i++) {
        uint boneIndex = 0;
        std::string boneName(aimesh->mBones[i]->mName.data);

        if (boneMapping.find(boneName) != boneMapping.end())
        {
            boneIndex = boneMapping[boneName];
        }
        else
        {
            boneIndex = numBones++;
            BoneInfo bone;
            bones.push_back(bone);

            boneMapping[boneName] = boneIndex;
            copyAiMat(&(aimesh->mBones[i]->mOffsetMatrix), bones[boneIndex].offsetMatrix);
        }

        for (uint j = 0; j < aimesh->mBones[i]->mNumWeights; j++)
        {
            uint vertexID = aimesh->mBones[i]->mWeights[j].mVertexId;
            float weight = aimesh->mBones[i]->mWeights[j].mWeight;
            addBoneDataToVBO(*meshPart, vertexID, boneIndex, weight);
        }
    }
}

void GLMesh::addBoneDataToVBO(MeshPart &meshPart, uint vertexID, uint boneID, float weight)
{
    uint boneDataIndexVBO = (vertexID + meshPart.baseVertex + 1) *
            (vertexStride / sizeof(float)) - AI_LMW_MAX_WEIGHTS * 2;

    auto& boneDataVBO = reinterpret_cast<BoneData &>(vertexElementsData[boneDataIndexVBO]);

    for (uint i = 0; i < AI_LMW_MAX_WEIGHTS; i++)
    {
        if (boneDataVBO.boneWeights[i] == 0.0f)
        {
            boneDataVBO.boneIDs[i] = boneID;
            boneDataVBO.boneWeights[i] = weight;
            return;
        }
    }
    // Not supposed to happen
    std::cout << "[Mesh] Run out of available bone weights!";
}

void GLMesh::copyAiNodes(const aiNode* node, MeshNode* targetNode)
{
    targetNode->transformation = glm::mat4();
    targetNode->name = node->mName.data;
    copyAiMat(&node->mTransformation, targetNode->transformation);
    if (node->mNumMeshes > 0)
    {
        targetNode->numMeshes = node->mNumMeshes;
        targetNode->meshes.resize(targetNode->numMeshes);

        for (int i = 0; i < targetNode->numMeshes; i++)
            targetNode->meshes[i] = node->mMeshes[i];
    }
    if (node->mChildren > 0)
    {
        targetNode->numChildren = node->mNumChildren;
        targetNode->children.resize(targetNode->numChildren);

        for (int i = 0; i < targetNode->numChildren; i++)
            copyAiNodes(node->mChildren[i], &targetNode->children[i]);
    }
}

void GLMesh::copyAiAnimations(const aiScene* scene)
{
    for (uint i = 0; i < scene->mNumAnimations; i++)
    {
        auto animation = scene->mAnimations[i];
        copyAiAnimation(animation, animations[animation->mName.data]);
    }
}

void GLMesh::copyAiAnimation(const aiAnimation* animation, Animation& targetAnimation)
{
    targetAnimation.duration = (uint)animation->mDuration;
    targetAnimation.ticksPerSecond = (uint)animation->mTicksPerSecond;

    for (uint j = 0; j < animation->mNumChannels; j++)
    {
        auto aichannel = animation->mChannels[j];
        auto& node = targetAnimation.nodes[aichannel->mNodeName.data];

        node.keyFramePerTick = ((animation->mDuration + 1) == aichannel->mNumPositionKeys &&
                                aichannel->mNumPositionKeys == aichannel->mNumScalingKeys &&
                                aichannel->mNumScalingKeys == aichannel->mNumRotationKeys);

        node.positionKeys.resize(aichannel->mNumPositionKeys);
        node.scalingKeys.resize(aichannel->mNumScalingKeys);
        node.rotationKeys.resize(aichannel->mNumRotationKeys);

        for (uint k = 0; k < aichannel->mNumPositionKeys; k++)
        {
            auto& aikey = aichannel->mPositionKeys[k];
            auto& key = node.positionKeys[k];

            key.time = (int)aikey.mTime;
            key.value = {aikey.mValue.x, aikey.mValue.y, aikey.mValue.z};
        }

        for (uint k = 0; k < aichannel->mNumScalingKeys; k++)
        {
            auto& aikey = aichannel->mScalingKeys[k];
            auto& key = node.scalingKeys[k];

            key.time = (int)aikey.mTime;
            key.value = {aikey.mValue.x, aikey.mValue.y, aikey.mValue.z};
        }

        for (uint k = 0; k < aichannel->mNumRotationKeys; k++)
        {
            auto& aikey = aichannel->mRotationKeys[k];
            auto& key = node.rotationKeys[k];

            key.time = (int)aikey.mTime;
            key.value = {aikey.mValue.w, aikey.mValue.x, aikey.mValue.y, aikey.mValue.z};
        }
    }
}

void GLMesh::calculateVertexStride()
{
    vertexStride = 3 * sizeof(float); // position (x, y, z)

    if (hasNormals)
        vertexStride += 3 * sizeof(float); // normals (x, y, z)

    for (int i = 0; i < numColorChannels; i++)
        vertexStride += 4 * sizeof(float); // colors (r, g, b, a)

    for (int i = 0; i < numUVChannels; i++)
        vertexStride += 2 * sizeof(float); // texture coordinates (u, v)

    if (hasBones)
    {
        // Bone IDs and weights
        vertexStride += sizeof(BoneData);
    }
}

void GLMesh::genVAO()
{
    glGenVertexArrays(1, &vertexArrayObject);
    glBindVertexArray(vertexArrayObject);

    // Buffer for vertex elements
    glGenBuffers(1, &vertexElementsBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexElementsBuffer);
    glBufferData(GL_ARRAY_BUFFER, vertexStride * numVertices, vertexElementsData.data(), GL_STATIC_DRAW);

    int attrib = 0, offset = 0;

    glEnableVertexAttribArray(attrib);
    glVertexAttribPointer(attrib++, 3, GL_FLOAT, GL_FALSE, vertexStride, reinterpret_cast<void*>(offset));
    offset += 3 * sizeof(float);

    if (hasNormals)
    {
        glEnableVertexAttribArray(attrib);
        glVertexAttribPointer(attrib++, 3, GL_FLOAT, GL_FALSE, vertexStride, reinterpret_cast<void*>(offset));
        offset += 3 * sizeof(float);
    }

    for (int i = 0; i < numColorChannels; i++)
    {
        glEnableVertexAttribArray(attrib);
        glVertexAttribPointer(attrib++, 4, GL_FLOAT, GL_FALSE, vertexStride, reinterpret_cast<void*>(offset));
        offset += 4 * sizeof(float);
    }

    for (int i = 0; i < numUVChannels; i++)
    {
        glEnableVertexAttribArray(attrib);
        glVertexAttribPointer(attrib++, 2, GL_FLOAT, GL_FALSE, vertexStride, reinterpret_cast<void*>(offset));
        offset += 2 * sizeof(float);
    }

    if (hasBones)
    {
        // Bone IDs
        glEnableVertexAttribArray(attrib);
        glVertexAttribIPointer(attrib++, 4, GL_INT, vertexStride, reinterpret_cast<void*>(offset));
        offset += 4 * sizeof(GLuint);
        // Bone weights
        glEnableVertexAttribArray(attrib);
        glVertexAttribPointer(attrib++, 4, GL_FLOAT, GL_FALSE, vertexStride, reinterpret_cast<void*>(offset));
        offset += 4 * sizeof(float);
    }


    // Buffer for faces
    glGenBuffers(1, &facesBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, facesBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * numFaces * 3, facesData.data(), GL_STATIC_DRAW);

    // Unbind buffers
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void GLMesh::render(RenderParams &renderParams)
{
    glBindVertexArray(vertexArrayObject);
    glBindBuffer(GL_ARRAY_BUFFER, vertexElementsBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, facesBuffer);

    currentProgramID = -1;
    render(renderParams, *rootNode);
    glBindVertexArray(0);
}

void GLMesh::render(RenderParams &renderParams, MeshNode& node)
{
    auto& modelMatrix = renderParams.matrices["modelMatrix"];
    // Save current modelMatrix and apply node's transformation
    auto parentModelMatrix = modelMatrix;
    modelMatrix *= node.transformation;

    // Draw all meshes assigned to this node
    for (int i=0; i < node.numMeshes; i++)
    {
        auto &meshPart = meshParts[node.meshes[i]];
        auto &material = materials[meshPart.indexMaterial];
        auto &shader = material.shader;

        if (material.textureDiffuse)
        {
            renderParams.samplers["diffuseMap"] = 0;
            material.textureDiffuse->bind();
        }

        if (shader->getProgramID() != currentProgramID)
        {
            shader->use();
            shader->setUniforms(renderParams);
            currentProgramID = shader->getProgramID();
        }
        else
        {
            shader->setUniform("modelMatrix", renderParams.matrices["modelMatrix"]);
        }

        glDrawElements(GL_TRIANGLES, meshPart.numFaces * 3, GL_UNSIGNED_INT,
                                     (void*)(meshPart.indexFaces * sizeof(GLuint)));
    }

#ifdef GL_MESH_FLATTENED
    // Process only top node children for speed up
    if (&node == rootNode.get())
    {
#endif
        // Draw all children
        for (int i=0; i < node.numChildren; i++)
        {
            render(renderParams, node.children[i]);
        }
#ifdef GL_MESH_FLATTENED
    }
#endif

    // Restore top node's modelMatrix
    modelMatrix = parentModelMatrix;
}

GLMesh::~GLMesh()
{

}
