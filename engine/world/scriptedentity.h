#ifndef SCRIPTEDENTITY_H
#define SCRIPTEDENTITY_H
#include "entity.h"
#include "engine/gfx/renderparams.h"

typedef void (*renderfunc)(void* objPtr, RenderParams renderParams);
typedef void (*updatefunc)(void* objPtr, float deltaTime);
typedef void (*destroyfunc)(void* objPtr);

class ScriptedEntity : public Entity
{
public:
    ScriptedEntity();
    ~ScriptedEntity();

    virtual void render(RenderParams renderParams);
    virtual void update(float deltaTime);

    void* objPtr;
    renderfunc renderCallback = nullptr;
    updatefunc updateCallback = nullptr;
    destroyfunc destroyCallback = nullptr;
};

#endif // SCRIPTEDENTITY_H
