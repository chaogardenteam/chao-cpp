#ifndef LEVEL_H
#define LEVEL_H
#include <memory>
#include "entity.h"
#include "engine/gfx/glmesh.h"
#include "engine/gfx/scenenode.h"
#include "engine/physics/terrainshape.h"
#include "engine/physics/rigidbody.h"

class Level : public Entity
{
    std::unique_ptr<SceneNode> node;
    std::shared_ptr<TerrainShape> shape;
    std::unique_ptr<RigidBody> rigidBody;
public:
    Level();
    ~Level();
};

#endif // LEVEL_H
