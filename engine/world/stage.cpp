#include "stage.h"
#include <Python.h>
#include "scripts/stage.h"

Stage::Stage(std::string stageName)
{
    this->stageName = stageName;
    PyInit_stage();
    objPtr = load_stage(stageName);
}

void Stage::loadTerrain()
{
    auto stageFolder = "levels/" + stageName + "/";
    auto packedFolder = stageFolder + "Packed/" + stageName + "/";

    auto terrainFilename = packedFolder + "terrain.terrain";
    auto blockFilename = packedFolder + "terrain-block.tbst";
    auto lightListFilename = packedFolder + "light-list.light-list";
    auto giInfoFilename = packedFolder + "gi-texture.gi-texture-group-info";

    auto autodraw_filename   = packedFolder + "Autodraw.txt";

    //auto terrain = new Terrain(terrainFilename, packedFolder);
    //auto terrain_gi_info  = new GITextureGroupInfo(gi_info_filename, terrain_cache_folder + "/");
    //auto terrain_block    = new TerrainBlock(block_filename);
    //auto light_list       = new LightList(light_list_filename);
    //auto terrain_autodraw = new TerrainAutodraw(autodraw_filename);

    //material_library = terrain->getMaterialLibrary();
}
