#ifndef ENTITY_H
#define ENTITY_H
#include <glm/glm.hpp>
#include "../gfx/renderparams.h"

class Entity
{
public:
    Entity();
    ~Entity();
    virtual void update(float deltaTime) {}
protected:
    glm::vec3 position = {0, 0, 0};
    glm::mat3 rotation;
    glm::vec3 scale = {1, 1, 1};
};

#endif // ENTITY_H
