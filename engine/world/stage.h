#ifndef STAGE_H
#define STAGE_H
#include <string>
#include "entity.h"


class Stage : public Entity
{
public:
    Stage(std::string stageName);

    void* objPtr;
    std::string stageName;
    void loadTerrain();
};

#endif // STAGE_H
