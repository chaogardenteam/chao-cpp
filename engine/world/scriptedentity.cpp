#include "scriptedentity.h"
#include <iostream>
#include <string>
#include "engine/engine.h"

ScriptedEntity::ScriptedEntity()
{
}

void ScriptedEntity::render(RenderParams renderParams)
{
    if (renderCallback != nullptr)
    {
        renderCallback(objPtr, renderParams);
    }
}

void ScriptedEntity::update(float deltaTime)
{
    if (updateCallback != nullptr)
    {
        updateCallback(objPtr, deltaTime);
    }
}

ScriptedEntity::~ScriptedEntity()
{
    if (destroyCallback != nullptr)
    {
        destroyCallback(objPtr);
    }
}
