#include "world.h"
#include "../gfx/renderparams.h"

World::World()
{
}

void World::add(std::shared_ptr<Entity> entity)
{
    entities.push_back(entity);
}

void World::update(float deltaTime)
{
    if (camera)
        camera->update(deltaTime);

    for (auto& entity : entities)
    {
        entity->update(deltaTime);
    }
}

World::~World()
{

}

