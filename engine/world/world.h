#ifndef WORLD_H
#define WORLD_H
#include <memory>
#include <vector>
#include "entity.h"
#include "camera.h"

class World
{
public:
    World();
    ~World();
    void add(std::shared_ptr<Entity> entity);
    void update(float deltaTime);
    std::shared_ptr<Entity> camera;
private:
    std::vector<std::shared_ptr<Entity>> entities;
};

#endif // WORLD_H
