#include "level.h"
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/postprocess.h>
#include "engine/resources.h"
#include "engine/gfx/glmesh.h"
#include "engine/physics/terrainshape.h"
#include "engine/engine.h"

Level::Level()
{
    auto engine = Engine::getInstance();

    auto mesh = Resources::getModel("level/chaostgneut/chaostgneut.dae", aiProcess_PreTransformVertices);
    //auto collisionMesh = Resources::getModel("level/chaostgneut/chaostgneut_invisible.dae", aiProcess_PreTransformVertices);
    node.reset(new SceneNode(mesh));
    engine->scene->add(node.get());

    shape = std::make_shared<TerrainShape>();
    shape->addModel(mesh);
    rigidBody.reset(new RigidBody(shape, 0.f));
    engine->physics->addRigidBody(rigidBody.get());
    engine->physics->serialize();
}

Level::~Level()
{

}

