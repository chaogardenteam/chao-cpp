#ifndef INPUTENGINE_H
#define INPUTENGINE_H
#include <array>

enum Keys { KEY_NONE,
            KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT,
            KEY_JUMP, KEY_ACTION,
            KEY_CAMERA_LEFT, KEY_CAMERA_RIGHT,
            KEY_ALWAYS_AT_END };

class InputEngine
{
public:
    InputEngine();
    void keyPressed(int key);
    void keyReleased(int key);
    void resetPressed();

    inline bool isKeyPressed(Keys key);
    inline bool isKeyHeld(Keys key);

    Keys keyFromGLFW(int key);

    std::array<bool, KEY_ALWAYS_AT_END> keysPressed;
    std::array<bool, KEY_ALWAYS_AT_END> keysHeld;
};

#endif // INPUTENGINE_H
