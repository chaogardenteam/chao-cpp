#include "engine.h"

Engine::Engine()
{

}

Engine* Engine::getInstance()
{
    static Engine instance;
    return &instance;
}

void Engine::init()
{
    /* Init graphics engine */
    graphics = new GLGraphics();
    input = new InputEngine();
    physics = new PhysicsEngine();
    physics->initPhysics();
    scriptEngine = new ScriptEngine();

    controller = new GameFlowController();
}

void Engine::setScene(Scene* scene)
{
    this->scene = scene;
}

void Engine::setWorld(World* world)
{
    this->world = world;
}

void Engine::render()
{
    graphics->clear();
    scene->render();
}

void Engine::update(float deltaTime)
{
    world->update(deltaTime);
    input->resetPressed();
}

Engine::~Engine()
{
    delete graphics;
    delete input;
    delete physics;
    delete controller;
    delete scriptEngine;
}
