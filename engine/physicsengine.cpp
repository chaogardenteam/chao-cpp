#include "physicsengine.h"
#include <cstdio>

PhysicsEngine::PhysicsEngine()
{
    // Modified from bullet/examples/HelloWorld/HelloWorld.cpp
    ///collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
    collisionConfiguration = new btDefaultCollisionConfiguration();

    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    dispatcher = new	btCollisionDispatcher(collisionConfiguration);

    ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
    overlappingPairCache = new btDbvtBroadphase();

    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    solver = new btSequentialImpulseConstraintSolver;

    dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher,overlappingPairCache,solver,collisionConfiguration);

    dynamicsWorld->setGravity(btVector3(0,-10,0));
}

void PhysicsEngine::initPhysics()
{

}

void PhysicsEngine::addRigidBody(RigidBody* rigidBody)
{
    dynamicsWorld->addRigidBody(rigidBody->body.get());
}

void PhysicsEngine::rayTest(glm::vec3 &rayFromWorld, glm::vec3 &rayToWorld, btCollisionWorld::RayResultCallback &resultCallback) {
    btVector3 _rayFromWorld(rayFromWorld.x, rayFromWorld.y, rayFromWorld.z);
    btVector3 _rayToWorld(rayToWorld.x, rayToWorld.y, rayToWorld.z);
    dynamicsWorld->rayTest(_rayFromWorld, _rayToWorld, resultCallback);
}


void PhysicsEngine::serialize()
{
    btDefaultSerializer* serializer = new btDefaultSerializer();
    dynamicsWorld->serialize(serializer);

    FILE* file = fopen("cppsonic.bullet","wb");
    fwrite(serializer->getBufferPointer(),serializer->getCurrentBufferSize(),1, file);
    fclose(file);
    serializer->finishSerialization();
    delete serializer;
}

PhysicsEngine::~PhysicsEngine()
{
    //delete dynamics world
    delete dynamicsWorld;

    //delete solver
    delete solver;

    //delete broadphase
    delete overlappingPairCache;

    //delete dispatcher
    delete dispatcher;

    delete collisionConfiguration;
}
