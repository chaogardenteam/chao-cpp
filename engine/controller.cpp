#include "controller.h"
#include <memory>
#include "engine.h"
#include "world/level.h"
#include "world/stage.h"
#include "world/scriptedentity.h"

#include "Python.h"
#include "scripts/entity.h"

#include <iostream>

GameFlowController::GameFlowController():
    world(new World),
    scene(new Scene)
{
    auto engine = Engine::getInstance();
    engine->setScene(scene.get());
    engine->setWorld(world.get());

    auto level = std::make_unique<Level>();
    world->add(std::move(level));

    //auto stage = std::make_shared<Stage>("sgtest");
    //world->add(stage);

    PyInit_entity();
    auto player = std::shared_ptr<ScriptedEntity>(create_entity("Player"));
    world->add(player);

    auto camera = std::shared_ptr<ScriptedEntity>(create_entity("Camera"));
    world->add(camera);
}

GameFlowController::~GameFlowController()
{
    std::cout << "GameFlowController destroy";
    auto engine = Engine::getInstance();
    engine->setScene(nullptr);
    engine->setWorld(nullptr);
}

