#include "scriptengine.h"
#include <Python.h>

ScriptEngine::ScriptEngine()
{
    Py_Initialize();
    PyRun_SimpleString(
       "import sys\n"
       "sys.path.append('scripts')\n"
    );
}

ScriptEngine::~ScriptEngine()
{
    Py_Finalize();
}
