#ifndef RESOURCES_H
#define RESOURCES_H
#include "gfx/glmesh.h"
#include "gfx/glshader.h"
#include "gfx/gltexture.h"
#include <map>
#include <memory>
#include <string>
#ifdef _WIN32
#include <algorithm>
#endif

class Resources
{
private:
    Resources() {}
    std::map<std::string, std::weak_ptr<GLMesh>> models;
    std::map<std::string, std::weak_ptr<GLShader>> shaders;
    std::map<std::string, std::weak_ptr<GLTexture>> textures;
public:
    static Resources& getInstance();
    template<typename T> static std::shared_ptr<T> get(std::map<std::string, std::weak_ptr<T>> &items,
                                      const std::string& filename);
    template<typename T> static std::shared_ptr<T> get(std::map<std::string, std::weak_ptr<T>> &items,
                                      const std::string& filename, int flags);

    static std::shared_ptr<GLMesh> getModel(const std::string &filename, int flags = 0);
    static std::shared_ptr<GLShader> getShader(const std::string &filename);
    static std::shared_ptr<GLTexture> getTexture(const std::string &filename);
};

template<typename T>
std::shared_ptr<T> Resources::get(std::map<std::string, std::weak_ptr<T>> &items,
                 const std::string& filename)
{
    auto iterator = items.find(filename);
    if (iterator != items.end())
        if (auto item = items[filename].lock())
            return item;

    auto item = std::make_shared<T>();
    items[filename] = item;
//#ifdef _WIN32
//    auto filenameWin = filename;
//    std::replace( filenameWin.begin(), filenameWin.end(), '/', '\\');
//    item->load(filenameWin);
//#else
    item->load(filename);
//#endif
    return item;
}

template<typename T>
std::shared_ptr<T> Resources::get(std::map<std::string, std::weak_ptr<T>> &items,
                 const std::string& filename, int flags)
{
    auto iterator = items.find(filename);
    if (iterator != items.end())
        if (auto item = items[filename].lock())
            return item;

    auto item = std::make_shared<T>();
    items[filename] = item;
    item->load(filename, flags);
    return item;
}

std::string getDirName(const std::string& filename);
std::string getBaseName(const std::string& filename);

#endif // RESOURCES_H
