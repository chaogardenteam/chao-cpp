#ifndef GAMEFLOWCONTROLLER_H
#define GAMEFLOWCONTROLLER_H
#include <memory>
#include "world/world.h"
#include "gfx/scene.h"

class GameFlowController
{
public:
    GameFlowController();
    ~GameFlowController();
private:
    std::unique_ptr<World> world;
    std::unique_ptr<Scene> scene;
};

#endif // GAMEFLOWCONTROLLER_H
