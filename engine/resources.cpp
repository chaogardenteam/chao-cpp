#include "resources.h"

Resources& Resources::getInstance()
{
    static Resources instance;
    return instance;
}

std::shared_ptr<GLMesh> Resources::getModel(const std::string& filename, int flags)
{
    auto &instance = getInstance();
    return instance.get(instance.models, filename, flags);
}

std::shared_ptr<GLShader> Resources::getShader(const std::string& filename)
{
    auto &instance = getInstance();
    return instance.get(instance.shaders, filename);
}

std::shared_ptr<GLTexture> Resources::getTexture(const std::string& filename)
{
    auto &instance = getInstance();
    return instance.get(instance.textures, filename);
}

std::string getDirName(const std::string& filename)
{
#ifdef _WIN32
    auto separator = filename.find_last_of("/\\");
#else
    auto separator = filename.find_last_of("/");
#endif
    if (separator > 0)
        return filename.substr(0, separator + 1);
    else
        return filename;
}

std::string getBaseName( std::string const& filename )
{
#ifdef _WIN32
    auto separator = filename.find_last_of("/\\");
#else
    auto separator = filename.find_last_of("/");
#endif
    auto ext = filename.find_last_of(".");

    if (separator > 0)
    {
        separator += 1;
        if (ext > separator)
            return filename.substr(separator, ext - separator);
        else
            return filename.substr(separator);
    }
    else
        return filename;
}
