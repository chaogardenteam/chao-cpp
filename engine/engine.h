#ifndef ENGINE_H
#define ENGINE_H
#include "gfx/glgraphics.h"
#include "gfx/scene.h"
#include "world/world.h"
#include "controller.h"
#include "inputengine.h"
#include "physicsengine.h"
#include "scriptengine.h"

class Engine
{
public:
    ~Engine();
    static Engine *getInstance();

    void init();
    void setScene(Scene* Scene);
    void setWorld(World* world);
    void render();
    void update(float deltaTime);

    GameFlowController *controller;
    GLGraphics* graphics;
    InputEngine* input = nullptr;
    PhysicsEngine* physics = nullptr;
    ScriptEngine* scriptEngine = nullptr;
    World* world = nullptr;
    // TODO: create generic scene base class
    Scene* scene = nullptr;

private:
    Engine();
};

#endif // ENGINE_H
