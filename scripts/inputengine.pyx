cdef class InputEngine:
    cpdef is_key_pressed(self, int key):
        return self.thisptr.keysPressed[key]

    cpdef is_key_held(self, int key):
        return self.thisptr.keysHeld[key]        