from gfxmodel cimport GfxModel

cdef class AnimationPlayer:
    def __cinit__(self, GfxModel model):
        self.thisptr = new CAnimationPlayer(model.thisptr)

    def __dealloc__(self):
        del self.thisptr

    cpdef start_animation(self, string name):
        self.thisptr.startAnimation(name)

    cpdef play_animation(self, float delta_time):
        self.thisptr.playAnimation(delta_time)
