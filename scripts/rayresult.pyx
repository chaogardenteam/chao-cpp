from defs.glm cimport distance
from defs.rayresult cimport ClosestRayResult as CClosestRayResult
from glm cimport vec3

inf = float("inf")

cdef class ClosestRayResult:
    def __init__(self, vec3 ray_from_world, vec3 ray_to_world):
        self.ray_from_world = ray_from_world
        self.ray_to_world = ray_to_world

    def __cinit__(self, vec3 ray_from_world, vec3 ray_to_world):
        self.result = new CClosestRayResult(ray_from_world.vec, ray_to_world.vec)

    def __dealloc__(self):
        del self.result

    def __repr__(self):
        return 'ClosestRayResult(ray_from_world={}, ray_to_world={}, distance={})'.format(
            self.ray_from_world, self.ray_to_world, self.distance)

    cdef void check_result(self):
        self.has_hit = has_hit = self.result.hasHit()
        if has_hit:
            vec = vec3(0, 0, 0)
            vec.vec = self.result.getHitPoint()
            self.hit_point = vec

            vec = vec3(0, 0, 0)
            vec.vec = self.result.getHitNormal()
            self.hit_normal = vec

    property distance:
        def __get__(self):
            if self.has_hit:
                return distance(self.ray_from_world.vec, self.hit_point.vec)
            else:
                return inf