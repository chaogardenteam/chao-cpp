from defs.engine cimport Engine as CEngine
from inputengine cimport InputEngine
from physicsengine cimport PhysicsEngine
from scene cimport Scene

cdef class Engine:
    def __cinit__(self):
        self.thisptr = CEngine.getInstance()

    property scene:
        def __get__(self):
            scene = Scene()
            scene.thisptr = self.thisptr.scene
            return scene

    property input:
        def __get__(self):
            input_engine = InputEngine()
            input_engine.thisptr = self.thisptr.input
            return input_engine

    property physics:
        def __get__(self):
            physics_engine = PhysicsEngine()
            physics_engine.thisptr = self.thisptr.physics
            return physics_engine            