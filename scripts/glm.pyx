from defs.glm cimport to_string
from libc.math cimport sqrt

cdef class mat3x3:
    def __cinit__(self, mat4x4 mat=None):
        if mat:
            self.mat = glm.mat3x3(mat.mat)

    def __mul__(mat3x3 x, vec3 y):
        vec = vec3(0, 0, 0)
        vec.vec = x.mat * y.vec
        return vec

    def __imul__(self, mat3x3 x):
        self.mat *= x.mat
        return self

    def __str__(self):
        return to_string(self.mat).decode()

    cpdef inverse(self):
        inversed = mat3x3()
        inversed.mat = glm.inverse(self.mat)
        return inversed   

    cpdef align_y_axis(self, vec3 target, float alpha):
        '''Returns a new y axis aligned Matrix'''
        cdef float x[3]
        cdef float y[3]
        cdef float z[3]
        cdef float mag

        # Y vector
        y[0] = target.vec.x * alpha + self.mat[1][0] * (1 - alpha)
        y[1] = target.vec.y * alpha + self.mat[1][1] * (1 - alpha)
        y[2] = target.vec.z * alpha + self.mat[1][2] * (1 - alpha)

        # normalize y
        mag = sqrt(y[0] * y[0] + y[1] * y[1] + y[2] * y[2])
        if (mag):
            y[0] /= mag
            y[1] /= mag
            y[2] /= mag

        # Z vector
        z[0] = self.mat[2][0]
        z[1] = self.mat[2][1]
        z[2] = self.mat[2][2]

        # X vector = Y cross Z
        x[0] = y[1] * z[2] - y[2] * z[1]
        x[1] = -y[0] * z[2] + y[2] * z[0]
        x[2] = y[0] * z[1] - y[1] * z[0]

        # normalize x
        mag = sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2])
        if (mag):
            x[0] /= mag
            x[1] /= mag
            x[2] /= mag

        # Recompute Z = X cross Y
        z[0] = x[1] * y[2] - x[2] * y[1]
        z[1] = -x[0] * y[2] + x[2] * y[0]
        z[2] = x[0] * y[1] - x[1] * y[0]

        # normalize z
        mag = sqrt(z[0] * z[0] + z[1] * z[1] + z[2] * z[2])
        if (mag):
            z[0] /= mag
            z[1] /= mag
            z[2] /= mag

        cdef mat3x3 mm = mat3x3()
        mm.mat[0][0] = x[0]
        mm.mat[0][1] = x[1]
        mm.mat[0][2] = x[2]
        mm.mat[1][0] = y[0]
        mm.mat[1][1] = y[1]
        mm.mat[1][2] = y[2]
        mm.mat[2][0] = z[0]
        mm.mat[2][1] = z[1]
        mm.mat[2][2] = z[2]

        return mm


cdef class mat4x4:
    def __cinit__(self, mat3x3 mat=None):
        if mat:
            self.mat = glm.mat4x4(mat.mat)

    def __imul__(self, mat4x4 x):
        self.mat *= x.mat
        return self

    def __str__(self):
        return to_string(self.mat).decode()

    def get(self):
        return (
            (self.mat[0][0], self.mat[0][1], self.mat[0][2], self.mat[0][3]),
            (self.mat[1][0], self.mat[1][1], self.mat[1][2], self.mat[1][3]),
            (self.mat[2][0], self.mat[2][1], self.mat[2][2], self.mat[2][3]),
            (self.mat[3][0], self.mat[3][1], self.mat[3][2], self.mat[3][3]))

    def set(self, mat):
        self.mat[0][0], self.mat[0][1], self.mat[0][2], self.mat[0][3] = mat[0]
        self.mat[1][0], self.mat[1][1], self.mat[1][2], self.mat[1][3] = mat[1]
        self.mat[2][0], self.mat[2][1], self.mat[2][2], self.mat[2][3] = mat[2]
        self.mat[3][0], self.mat[3][1], self.mat[3][2], self.mat[3][3] = mat[3]

    cpdef inverse(self):
        inversed = mat4x4()
        inversed.mat = glm.inverse(self.mat)
        return inversed 


cdef class vec:
    pass

cdef class vec2(vec):
    def __cinit__(self, float x, float y):
        self.vec = glm.vec2(x, y)

    property x:
        def __get__(self):
            return self.vec.x
        def __set__(self, float val):
            self.vec.x = val

    property y:
        def __get__(self):
            return self.vec.y
        def __set__(self, float val):
            self.vec.y = val

    def __add__(vec2 x, vec2 y):
        vec = vec2(0, 0)
        vec.vec = x.vec + y.vec
        return vec

    def __sub__(vec2 x, vec2 y):
        vec = vec2(0, 0)
        vec.vec = x.vec - y.vec
        return vec        

    def __mul__(vec2 x, float y):
        vec = vec2(0, 0)
        vec.vec = x.vec * y
        return vec

    def __iadd__(self, vec2 x):
        self.vec += x.vec
        return self

    def __imul__(self, vec2 x):
        self.vec *= x.vec
        return self

    def __str__(self):
        return to_string(self.vec).decode()        

    cpdef dot(self, vec2 x):
        return glm.dot(self.vec, x.vec)

    cpdef length(self):
        return glm.length(self.vec)        


cdef class vec3(vec):
    def __cinit__(self, float x, float y, float z):
        self.vec = glm.vec3(x, y, z)     

    property x:
        def __get__(self):
            return self.vec.x
        def __set__(self, float val):
            self.vec.x = val

    property y:
        def __get__(self):
            return self.vec.y
        def __set__(self, float val):
            self.vec.y = val

    property z:
        def __get__(self):
            return self.vec.z
        def __set__(self, float val):
            self.vec.z = val

    property xz:
        def __get__(self):
            return vec2(self.vec.x, self.vec.z)

    def __add__(vec3 x, vec3 y):
        vec = vec3(0, 0, 0)
        vec.vec = x.vec + y.vec
        return vec

    def __sub__(vec3 x, vec3 y):
        vec = vec3(0, 0, 0)
        vec.vec = x.vec - y.vec
        return vec        

    def __mul__(vec3 x, float y):
        vec = vec3(0, 0, 0)
        vec.vec = x.vec * y
        return vec

    def __iadd__(self, vec3 x):
        self.vec += x.vec
        return self

    def __imul__(self, vec3 x):
        self.vec *= x.vec
        return self

    def __str__(self):
        return to_string(self.vec).decode() 

    cpdef dot(self, vec3 x):
        return glm.dot(self.vec, x.vec)

    cpdef length(self):
        return glm.length(self.vec)

    cpdef normalize(self):
        vec = vec3(0, 0, 0)
        vec.vec = glm.normalize(self.vec)
        return vec


cdef class vec4(vec):
    pass

cpdef rotate(mat4x4 m, float angle, vec3 axis):
    rotated = mat4x4()
    rotated.mat = glm.rotate(m.mat, angle, axis.vec)
    return rotated    

cpdef scale(mat4x4 m, vec3 v):
    cdef glm.mat4x4 m_scaled = glm.scale(m.mat, v.vec)
    scaled = mat4x4()
    scaled.mat = m_scaled
    return scaled

cpdef translate(mat4x4 m, vec3 v):
    cdef glm.mat4x4 m_translated = glm.translate(m.mat, v.vec)
    translated = mat4x4()
    translated.mat = m_translated
    return translated

cpdef look_at(vec3 eye, vec3 center, vec3 up):
    mat = mat4x4()
    mat.mat = glm.lookAt(eye.vec, center.vec, up.vec)
    return mat

cpdef perspective(float fovy, float aspect, float z_near, float z_far):
    mat = mat4x4()
    mat.mat = glm.perspective(fovy, aspect, z_near, z_far)
    return mat
