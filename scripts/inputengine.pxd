from defs.inputengine cimport InputEngine as CInputEngine

cpdef enum Keys:
    KEY_NONE,
    KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT,
    KEY_JUMP, KEY_ACTION,
    KEY_CAMERA_LEFT, KEY_CAMERA_RIGHT,
    KEY_ALWAYS_AT_END


cdef class InputEngine:
    cdef CInputEngine* thisptr

    cpdef is_key_pressed(self, int key)
    cpdef is_key_held(self, int key)