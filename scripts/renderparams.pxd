from libcpp.vector import vector
from defs.renderparams cimport RenderParams as CRenderParams
from glm cimport mat4x4
from animationplayer cimport AnimationPlayer

cdef class RenderParams:
    cdef CRenderParams params
    cpdef set_matrix(self, bytes name, mat4x4 mat)
    cpdef set_bone_transforms(self, AnimationPlayer player)
