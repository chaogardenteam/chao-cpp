from libcpp.string cimport string
import glm
import xml.etree.ElementTree as ET

cdef public class Stage[object PyStage, type PyStageType]:
    cdef effects
    cdef lights
    cdef spawn_info
    cdef sky_info
    cdef terrain_info

    def __init__(self, stage_name):
        path = 'level/{}/'.format(stage_name)
        self.load_stage(path)
        self.load_effects(path)
        self.load_terrain(path)

    def load_stage(self, path):
        tree = ET.parse(path + 'Stage.stg.xml')
        root = tree.getroot()

        for el in root:
            if el.tag == 'Sonic':
                self.spawn_info = {
                    'Position': get_vec3(el.find('Position')),
                    'Yaw': get_int(el.find('Yaw')),
                    'DeadHeight': get_int(el.find('DeadHeight')),
                }

    def load_effects(self, path):
        tree = ET.parse(path + 'SceneEffect.prm.xml')
        root = tree.getroot()

        self.effects = effects = {}
        for el in root:
            effect = effects[el.tag] = {}

            for params_type in el.find('Category'):
                effect[params_type.tag] = {
                    param.tag: param.text for param in params_type[0]
                }

    def load_terrain(self, path):
        tree = ET.parse(path + 'Terrain.stg.xml')
        root = tree.getroot()

        self.terrain_info = terrain = {}
        self.sky_info = sky = {}
        self.lights = lights = []

        for el in root:
            if el.tag == 'Terrain':
                terrain.update({
                    param.tag: param.text for param in el
                })
            elif el.tag == 'Sky':
                sky.update({
                    param.tag: param.text for param in el
                })
            elif el.tag == 'Light':
                lights.append({
                    param.tag: param.text for param in el
                })


        print(terrain)

def get_int(el):
    return int(el.text)

def get_vec3(el):
    return glm.vec3(*map(lambda x: int(x.text), el[:3]))


cdef public void* load_stage(string stage_name):
    stage = Stage(stage_name.decode())
    return <void*>stage
