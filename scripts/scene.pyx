from defs.scene cimport Scene as CScene
from scenenode cimport SceneNode


cdef class Scene:
    cpdef add(self, SceneNode node):
        self.thisptr.add(node.thisptr)

    property camera_node:
        def __get__(self):
            node = SceneNode()
            node.thisptr = self.thisptr.cameraNode.get()
            return node