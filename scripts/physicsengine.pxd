from defs.physicsengine cimport PhysicsEngine as CPhysicsEngine
from glm cimport vec3
from rayresult cimport ClosestRayResult

cdef class PhysicsEngine:
    cdef CPhysicsEngine* thisptr

    cpdef ray_test(self, vec3 ray_from_world, vec3 ray_to_world, max_distance=*)