from libcpp_ext.memory cimport shared_ptr
from defs.glmesh cimport GLMesh
from gfxmodel cimport GfxModel

cdef class SceneNode:
    def __cinit__(self, GfxModel model=None):
        if model:
            self.thisptr = new CSceneNode(model.thisptr)
            self.is_created = True

    def __dealloc__(self):
        if self.is_created:
            del self.thisptr

    cpdef set_render_params(self, RenderParams params):
        self.thisptr.setRenderParams(params.params)