from libcpp_ext.memory cimport shared_ptr, unique_ptr
from defs.glm cimport vec3

cdef extern from "engine/physics/rigidbody.h":
    cppclass CollisionShape
    cppclass RigidBody:
        RigidBody(shared_ptr[CollisionShape] shape, float mass)