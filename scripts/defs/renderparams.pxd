from libcpp.map cimport map
from libcpp.vector cimport vector
from libcpp.string cimport string
from libcpp_ext.memory cimport shared_ptr, unique_ptr
from defs.glmesh cimport GLMesh
from defs.glm cimport mat4x4, vec4
from defs.renderparams cimport RenderParams

cdef extern from "engine/gfx/renderparams.h":
    cppclass RenderParams:
        map[string, mat4x4] matrices
        map[string, vector[mat4x4]] matrixArrays
        map[string, int] samplers
        map[string, vec4] vectors
