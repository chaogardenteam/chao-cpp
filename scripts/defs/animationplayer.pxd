from libcpp_ext.memory cimport shared_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from defs.glm cimport mat4x4
from defs.glmesh cimport GLMesh


cdef extern from "engine/gfx/animationplayer.h":
    cppclass AnimationPlayer:
        AnimationPlayer(shared_ptr[GLMesh] mesh)
        vector[mat4x4] boneTransforms
        void playAnimation(float deltaTime)
        void startAnimation(string animationName)
