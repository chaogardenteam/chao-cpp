from defs.glm cimport vec3
from libcpp cimport bool

cdef extern from "engine/physics/rayresult.h":
    cppclass ClosestRayResult:
        ClosestRayResult(vec3& rayFromWorld, vec3& rayToWorld)
        bool hasHit()
        vec3 getHitPoint()
        vec3 getHitNormal()