from libcpp.string cimport string
from libcpp_ext.memory cimport shared_ptr
from defs.glmesh cimport GLMesh

cdef extern from "engine/resources.h":
    cppclass GLShader
    cppclass GLTexture

    cppclass Resources:
        @staticmethod
        Resources& getInstance()
        @staticmethod
        shared_ptr[GLMesh] getModel(const string &filename, int flags)
        @staticmethod
        shared_ptr[GLShader] getShader(const string &filename)
        @staticmethod
        shared_ptr[GLTexture] getTexture(const string &filename)