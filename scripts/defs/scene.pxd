from defs.scenenode cimport SceneNode
from libcpp_ext.memory cimport unique_ptr

cdef extern from "engine/gfx/scene.h":
    cppclass Scene:
        void add(SceneNode* node)
        unique_ptr[SceneNode] cameraNode