from libcpp.string cimport string
from libcpp_ext.memory cimport shared_ptr, unique_ptr
from defs.glmesh cimport GLMesh
from defs.renderparams cimport RenderParams

cdef extern from "engine/gfx/scenenode.h":
    cppclass SceneNode:
        SceneNode(shared_ptr[GLMesh] model)
        unique_ptr[RenderParams] renderParams
        void setRenderParams(RenderParams &params)
        void updateRenderParams()
        void render(RenderParams &parentParams)
