from libcpp.string cimport string

cdef extern from "<glm/vec2.hpp>" namespace "glm":
    cppclass vec2:
        vec2()
        vec2(float x, float y)
        float x, y
        vec2 operator*(float s)
        vec2 operator+(vec2 &v)
        vec2 operator-(vec2 &v)        


cdef extern from "<glm/vec3.hpp>" namespace "glm":
    cppclass vec3:
        vec3()
        vec3(float x, float y, float z)
        float x, y, z
        vec3 operator*(float s)
        vec3 operator+(vec3 &v)
        vec3 operator-(vec3 &v)

cdef extern from "<glm/vec4.hpp>" namespace "glm":
    cppclass vec4:
        pass
    
cdef extern from "<glm/mat3x3.hpp>" namespace "glm":
    cppclass mat3x3:
        mat3x3 mat3x3()
        mat3x3 mat3x3(mat4x4 &m)    
        mat3x3 operator*(mat3x3 &m2)
        vec3 operator*(vec3 &v)
        float* operator[](int i)
    mat3x3 inverse(mat3x3 &m)
    mat3x3 transpose(mat3x3 &m)

cdef extern from "<glm/mat4x4.hpp>" namespace "glm":        
    cppclass mat4x4:
        mat4x4 mat4x4()
        mat4x4 mat4x4(mat3x3 &m)
        mat4x4 operator*(mat4x4 &m2)
        vec4 operator*(vec4 &v)
        float* operator[](int i)        
    mat4x4 inverse(mat4x4 &m)
    mat3x3 transpose(mat4x4 &m)

cdef extern from "<glm/geometric.hpp>" namespace "glm":
    float distance(vec3 &p0, vec3 &p1)
    float length(vec2 &x)
    float length(vec3 &x)
    float dot(vec2 &x, vec2 &y)
    float dot(vec3 &x, vec3 &y)
    vec3 normalize(vec3 &x)

cdef extern from "<glm/gtc/matrix_transform.hpp>" namespace "glm":
    mat4x4 lookAt(vec3 &eye, vec3 &center, vec3 &up)
    mat4x4 rotate(mat4x4 &m, float angle, vec3 &axis)
    mat4x4 scale(mat4x4 &m, vec3 &v)
    mat4x4 perspective(float &fovy, float &aspect, float &zNear, float &zFar)
    mat4x4 translate(mat4x4 &m, vec3 &v)

cdef extern from "<glm/gtx/string_cast.hpp>" namespace "glm":
    string to_string(vec2 &x)
    string to_string(vec3 &x)
    string to_string(vec4 &x)
    string to_string(mat3x3 &x)  
    string to_string(mat4x4 &x)
 
