from defs.glm cimport vec3
from defs.rigidbody cimport RigidBody
from defs.rayresult cimport ClosestRayResult

cdef extern from "engine/physicsengine.h":
    cppclass PhysicsEngine:
        void initPhysics()
        void addRigidBody(RigidBody* rigidBody)
        void rayTest(vec3 &rayFromWorld, vec3 &rayToWorld, ClosestRayResult &resultCallback)
        void serialize()
