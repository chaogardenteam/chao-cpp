from libcpp cimport bool
from cython cimport uint
from libcpp.map cimport map
from libcpp.vector cimport vector
from libcpp.string cimport string
from libcpp_ext.memory cimport unique_ptr, shared_ptr

cdef extern from "<glm/vec4.hpp>" namespace "glm":
    cppclass vec4    
    
cdef extern from "<glm/mat4x4.hpp>" namespace "glm":
    cppclass mat4

cdef extern from "engine/gfx/renderparams.h":
    struct RenderParams:
        map[string, mat4] matrices;
        map[string, vector[mat4]] matrixArrays;
        map[string, int] samplers;
        map[string, vec4] vectors;

cdef extern from "engine/gfx/gltexture.h":
    cppclass GLTexture
    
cdef extern from "engine/gfx/glshader.h":
    cppclass GLShader

cdef extern from "engine/gfx/glmesh.h":
    ctypedef int GLuint
    ctypedef float GLfloat
    
    cppclass MeshNode:
        string name
        int numChildren
        int numMeshes
        vector[MeshNode] children
        vector[uint] meshes
        mat4 transformation

    struct MeshPart:
        int numFaces
        int numVertices
        int indexFaces
        int indexVertices
        int indexMaterial
        int baseVertex

    struct BoneInfo:
        mat4 offsetMatrix;

    struct BoneData:
        int boneIDs[4]
        float boneWeights[4]

    struct AnimationKey:
        int time
        #glm::vec3 value = glm::vec3();

    struct AnimationRotationKey:
        int time
        #glm::quat value = glm::quat();

    struct AnimationNode:
        bool keyFramePerTick
        vector[AnimationKey] positionKeys
        vector[AnimationKey] scalingKeys
        vector[AnimationRotationKey] rotationKeys
        vector[mat4] transforms

    struct Animation:
        int duration
        int ticksPerSecond
        map[string, AnimationNode] nodes

    struct Material:
        pass
        shared_ptr[GLTexture] textureDiffuse;
        shared_ptr[GLShader] shader;

    cppclass GLMesh:
        bool load(const string &filename, int flags);
        bool loadAnimation(const string &filename, const string animationName, int flags);
        void render(RenderParams& renderParams, MeshNode &node);
        void render(RenderParams& renderParams);
        void genVAO()
        void calculateVertexStride()

        string filename;
        string dirname;

        int numBones
        map[string, int] boneMapping;
        vector[BoneInfo] bones;
        map[string, Animation] animations;
        unique_ptr[MeshNode] rootNode;

        bool hasNormals
        bool hasBones
        int numVertices
        int numFaces
        int numColorChannels
        int numUVChannels
        int numMeshParts
        int numMaterials

        vector[MeshPart] meshParts;
        vector[Material] materials;
        vector[GLfloat] vertexElementsData;
        vector[GLuint] facesData;

    #private:
        #GLuint vertexArrayObject = 0;
        #GLuint vertexElementsBuffer = 0;
        #GLuint facesBuffer = 0;
        #int indexVBO = 0;
        #int indexIBO = 0;

        #int vertexStride = 3 * sizeof(float);
        #shared_ptr<GLShader> shader;
        #GLuint currentProgramID = -1;

        #void copyAiMaterials(const aiScene *scene);
        #void copyAiMeshes(const aiScene *scene);
        #void copyAiNodes(const aiNode *node, MeshNode *targetNode);
        #void copyAiBones(MeshPart *meshPart, const aiMesh *aimesh);
        #void copyAiAnimations(const aiScene *scene);
        #void copyAiAnimation(const aiAnimation *animation, Animation &target);
        #void addBoneDataToVBO(MeshPart &meshPart, uint vertexID, uint boneIndex, float weight);
        #void calculateVertexStride();
