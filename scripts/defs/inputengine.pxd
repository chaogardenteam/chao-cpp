from libcpp cimport bool

cdef extern from "engine/inputengine.h":
    cppclass InputEngine:
        bool isKeyPressed(int key)
        bool isKeyHeld(int key)

        bool* keysPressed;
        bool* keysHeld;