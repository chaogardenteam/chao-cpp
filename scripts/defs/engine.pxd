cdef extern from "engine/engine.h":
    cppclass GameFlowController
    cppclass GLGraphics
    cppclass InputEngine
    cppclass PhysicsEngine
    cppclass World
    cppclass Scene

    cppclass Engine:
        @staticmethod
        Engine* getInstance()
        void setScene(Scene* Scene);
        void setWorld(World* world);

        GameFlowController *controller
        GLGraphics* graphics
        InputEngine* input
        PhysicsEngine* physics
        World* world
        Scene* scene