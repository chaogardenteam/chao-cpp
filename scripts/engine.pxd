from defs.engine cimport Engine as CEngine
from scene cimport Scene

cdef class Engine:
    cdef CEngine* thisptr
    cdef Scene _scene