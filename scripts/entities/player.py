import math
import glm
import inputengine
from glm import vec3, mat3x3, mat4x4
from entity import PyEntity
from engine import Engine
from scenenode import SceneNode
from resources import Resources
from animationplayer import AnimationPlayer
from renderparams import RenderParams
from entities.camera import Camera

scale = 1.0 / 40.0
acc = 0.046875 * 3600.0 * scale     # Acceleration
air = 0.09375 * 3600.0 * scale      # Air Acceleration
dec = 0.5 * 3600.0 * scale          # Deceleration
maxsp = 6 * 60.0 * scale            # Max speed
frc = 0.046875 * 3600.0 * scale     # Friction
grv = 0.21875 * 3600.0 * scale      # Gravity
jmp = 6.5 * 60.0 * scale            # Jump Stength
ljp = 4.0 * 60.0 * scale            # Low Jump Strength (for variable jump)
slp = 0.125 * 3600.0 * scale        # Slope Acceleration
mgs = 2.5 * 60.0 * scale            # Minimum Ground Speed to Stick on Walls
mrs = 1.03125 * 60.0 * scale        # Mininum Rolling Speed
mrr = 0.5 * 60.0 * scale            # Mininum Rolling Speed After Already Rolling
sru = 0.078125 * 60.0 * 60.0 * scale
srd = 0.3125 * 60.0 * 60.0 * scale

SPEED_BASED_ANIMATION_GAP = 0.2

STATE_NORMAL = 1
STATE_JUMPING = 2
STATE_ROLLING = 3
STATE_CROUCH_DOWN = 4
STATE_SPINDASH = 5
STATE_LOOK_UP = 6
STATE_SPRING = 7

moonJump = True
aiProcess_FlipUVs = 0x800000

animations = {
    "stopped": b"sn_idle00",
    "walking": b"sn_walk_loop",
    "running": b"sn_walk_loop",
    "spinning": b"sn_walk_loop",
}


class Player(PyEntity):

    def __init__(self):
        self.model = model = Resources().getModel(b"model/Sonic/Sonic.fbx", aiProcess_FlipUVs)
        self.model_dir = "model/Sonic"

        self.node = node = SceneNode(model)
        engine = Engine()
        engine.scene.add(node)
        self.input = engine.input
        self.physics = engine.physics

        self.animation_player = AnimationPlayer(model)
        self.load_animations()

        self.position = vec3(22.93 * 0.15, 15 * 0.15, 14.30883 * 0.15)
        self.rotation = mat3x3()
        self.scale = vec3(1, 1, 1)
        self.speed = vec3(0, 0, 0)

        self.on_ground = False
        self.on_ledge = False
        self.is_holding_jump = False

        self.current_animation = "stopped"
        self.last_animation = None
        self.animation_transform = None

        self.time_waiting = 0.0
        self.is_pushing = False

        self.state = STATE_NORMAL

        self.spindash_charge = 0

        self.direction = vec3(0, 0, 0)

        self.angle = 0.0
        self.angle_override = False
        self.camera_rotation = None
        self.time = 0

        # FIXME: Need a better interface to attach camera to player
        Camera.player = self

    def load_animations(self):
        model = self.model
        model_dir = self.model_dir

        for animation in set(animations.values()):
            animation_file = '{}/{}.fbx'.format(model_dir, animation.decode()).encode()
            model.load_animation(animation_file, animation)

    def update(self, delta_time):
        self.time += delta_time
        self.handle_controls(delta_time)

        if self.on_ground:
            self.handle_ground(delta_time)
        else:
            self.handle_air(delta_time)

        self.reposition_player(delta_time)
        self.handle_collisions(delta_time)
        self.handle_animations(delta_time)

        self.animation_player.play_animation(delta_time)
        self.set_render_params()

    def set_render_params(self):
        render_params = RenderParams()
        # Move Sonic model lower since it's not centered
        model_matrix = glm.translate(glm.mat4x4(), self.position + vec3(0, -20 * scale, 0))

        model_matrix *= glm.rotate(mat4x4(self.rotation), self.angle, vec3(0, 1, 0))
        model_matrix *= self.animation_transform

        model_matrix = glm.scale(model_matrix, self.scale)

        render_params.set_matrix(b"modelMatrix", model_matrix)
        render_params.set_bone_transforms(self.animation_player)

        self.node.set_render_params(render_params)

    def convert_motion_to_air(self):
        self.speed = self.rotation * self.speed
        self.on_ground = False

    def convert_motion_to_ground(self):
        self.speed = self.rotation.inverse() * self.speed
        self.on_ground = True

    def align_player_to_normal(self, normal, delta_time):
        if self.speed.length() < mgs:
            # print(self.rotation)
            self.rotation = self.rotation.align_y_axis(
                normal, 1.0 - math.pow(0.0001, delta_time))
            # print(normal, self.rotation, 1.0 - math.pow(0.0001, delta_time))
        else:
            self.rotation = self.rotation.align_y_axis(normal, 1.0)

    def align_controls_to_camera(self):
        # Controls aligned to camera
        move_x = vec3(1.0, 0.0, 0.0)
        move_z = vec3(0.0, 0.0, 1.0)

        if self.camera_rotation:
            move_x = self.camera_rotation * move_x
            move_z = self.camera_rotation * move_z

        move_x.y = 0.0
        move_z.y = 0.0

        self.move_x = move_x.normalize()
        self.move_z = move_z.normalize()

    def handle_controls(self, delta_time):
        self.time_waiting += delta_time

        self.can_move = True
        self.align_controls_to_camera()

        move_x = self.move_x
        move_z = self.move_z

        self.direction = direction = vec3(0, 0, 0)
        speed = self.speed
        state = self.state

        pinput = self.input

        if pinput.is_key_held(inputengine.KEY_LEFT):
            direction -= move_x

        if pinput.is_key_held(inputengine.KEY_RIGHT):
            direction += move_x

        if pinput.is_key_held(inputengine.KEY_UP):
            direction -= move_z

        if pinput.is_key_held(inputengine.KEY_DOWN):
            direction += move_z

        if state in [STATE_CROUCH_DOWN, STATE_SPINDASH, STATE_LOOK_UP]:
            self.can_move = False

        if state != STATE_ROLLING:
            if direction.length() > 0 and self.can_move:

                self.time_waiting = 0

                # The player is holding the arrow keys on some direction
                self.direction = direction = direction.normalize()

                if vec3(speed.x, 0, speed.z).dot(direction) >= 0:
                    # The direction pressed is similar to the direction Sonic's facing

                    if speed.xz.length() > 2 * scale and not self.angle_override:
                        self.angle = math.atan2(speed.x, speed.z)
#                        console.log(angle)

                    # Store the old speed (for max speed check)
                    old_speed_modulus = speed.xz.length()

                    if self.on_ground:
                        # Ground acc
                        speed += direction * (acc * delta_time)
                    else:
                        # Air acc
                        speed += direction * (air * delta_time)

                    # Store the new speed
                    new_speed_modulus = speed.xz.length()

                    if new_speed_modulus > maxsp:
                        # We're faster than the max speed
                        if old_speed_modulus < maxsp:
                            # We got here by "natural means". Get back to the max speed, but allow handling
                            speed.x = (speed.x / new_speed_modulus) * maxsp
                            speed.z = (speed.z / new_speed_modulus) * maxsp
                        else:
                            # We got here by "supernatural means" (e.g. a Spring).
                            if new_speed_modulus > old_speed_modulus:
                                # We're faster than we were before: Get back to as fast
                                # as we already were, but allow handling
                                speed.x = (speed.x / new_speed_modulus) * old_speed_modulus
                                speed.z = (speed.z / new_speed_modulus) * old_speed_modulus

                else:
                    # The direction pressed is opposite to the direction Sonic's facing
                    if self.on_ground:
                        # Ground deceleration
                        speed += direction * (dec * delta_time)
                    else:
                        # Air deceleration ( = the same as acceleration)
                        speed += direction * (air * delta_time)

            else:
                # The player is not holding on any direction
                if self.on_ground:
                    # We're on the floor. Enters friction.
                    self.direction = direction = vec3(-speed.x, 0, -speed.z)
                    if direction.length():
                        self.direction = direction = direction.normalize()

                    speed += direction * (frc * delta_time)

                    # Stop when speed is lower than friction
                    if speed.length() < (frc * delta_time):
                        self.speed = speed = vec3(0, 0, 0)
                else:
                    # no friction on air (we calculate air drag later)
                    pass

        else:
            # Rolling physics

            # Reduced friction.
            frictionDirection = vec3(-speed.x, 0, -speed.z)
            if frictionDirection.length():
                frictionDirection = frictionDirection.normalize()
            speed += frictionDirection * (frc / 2 * delta_time)

            # Stop when speed is lower than friction
            if speed.length() < (frc * delta_time):
                speed = vec3(0, 0, 0)
            else:
                # Deceleration
                if vec3(speed.x, 0, speed.z).dot(direction) < 0:
                    speed += direction * (dec / 4 * delta_time)

        # Jump/Spindash/Spindash Rev
        if (self.on_ground or moonJump) and pinput.is_key_pressed(inputengine.KEY_JUMP):
            if state == STATE_NORMAL or state == STATE_ROLLING or (
                    moonJump and state == STATE_JUMPING):
                self.is_holding_jump = True
                speed.y = jmp
                self.on_ground = False
                self.time_waiting = 0
                self.state = state = STATE_JUMPING
            elif state == STATE_CROUCH_DOWN:
                self.state = state = STATE_SPINDASH
                self.spindashCharge = 0
            elif state == STATE_SPINDASH:
                self.spindashCharge += 2
                if self.spindashCharge > 8:
                    self.spindashCharge = 8

        # Variable jump
        if not pinput.is_key_held(inputengine.KEY_JUMP) and self.is_holding_jump:
            self.is_holding_jump = False
            if speed.y > ljp:
                speed.y = ljp

        # Roll/Crouch
        if pinput.is_key_held(inputengine.KEY_ACTION):
            self.time_waiting = 0
            # Only roll or crouch if we're on ground, and on normal state
            if self.on_ground and state == STATE_NORMAL or state == STATE_CROUCH_DOWN:
                # We're moving. Roll
                if speed.xz.length() > mrs:
                    if state != STATE_ROLLING:
                        self.state = state = STATE_ROLLING

                else:
                    # We're not moving. Crouch.
                    if state != STATE_SPINDASH:
                        self.state = state = STATE_CROUCH_DOWN

        else:
            # We've released the crouch button
            if state == STATE_CROUCH_DOWN:
                # We're crouching. Get back to normal.
                self.state = state = STATE_NORMAL

            if state == STATE_SPINDASH:
                # We're spindashing. Roll at the right speed.
                self.state = state = STATE_ROLLING
                speed.x = (8 + math.floor(math.floor(self.spindash_charge) / 2)) * 60 * math.sin(self.angle)
                speed.z = (8 + math.floor(math.floor(self.spindash_charge) / 2)) * 60 * math.cos(self.angle)

                self.spindash_charge *= math.pow(0.148834266, delta_time)

        # if self.input.pressed("R"):
        #     self.ringLoss()

    def handle_ground(self, delta_time):
        normal = self.rotation * vec3(0, 1, 0)
        speed = self.speed

        # Slope Acceleration
        if self.state != STATE_ROLLING:
            speed += vec3(normal.x * slp * delta_time, 0, normal.z * slp * delta_time)
        else:
            if vec3(normal.x, 0, normal.z).dot(vec3(speed.x, 0, speed.z)) >= 0:
                # Rolling Downhill
                speed += vec3(normal.x * srd * delta_time, 0, normal.z * srd * delta_time)
            else:
                # Rolling Uphill
                speed += vec3(normal.x * sru * delta_time, 0, normal.z * sru * delta_time)

        # Fall off from the walls if we're too slow
        if normal.dot(vec3(0, 1, 0)) <= 0.0 and speed.xz.length() < mgs:
            self.convert_motion_to_air()
            self.rotation = mat3x3()
            return

        # Stop rolling if we're too slow.
        if speed.length() <= mrr and self.state == STATE_ROLLING:
            self.state = STATE_NORMAL

    def handle_air(self, delta_time):
        # Add gravity to the player's y speed
        speed = self.speed
        speed.y -= grv * delta_time
        speed_y = speed.y

        if speed_y > 0 and speed_y < 4 * 60 * scale:
            # We're going upwards, but no that much
            if speed.xz.length() > 0.125 * 60.0 * scale:
                # We're going fast. Air drag kicks in
                speed.x *= math.pow(0.148834266 * scale, delta_time)
                speed.z *= math.pow(0.148834266 * scale, delta_time)

        if self.state == STATE_ROLLING:
            self.state = STATE_JUMPING
        elif self.state == STATE_SPRING:
            if speed_y < 0:
                self.state = STATE_NORMAL

        self.speed = speed

    def handle_collisions(self, delta_time):
        did_push = False
        physics = self.physics
        position = self.position
        rotation = self.rotation

        # Check collision with walls
        for i in frange(0, 2 * math.pi, 0.25 * math.pi):
            sensor_direction = vec3(math.sin(i), 0, math.cos(i))
            ray_from_world = position + vec3(0, 0, -4 * scale)
            ray_to_world = rotation * sensor_direction

            intersect = physics.ray_test(ray_from_world, ray_to_world, 15 * scale)
            speed_сomponent = sensor_direction.xz.dot(self.speed.xz)

            if intersect.has_hit:  # math.isfinite(intersect.distance):
                if speed_сomponent >= 0:
                    self.speed.x -= speed_сomponent * sensor_direction.x
                    self.speed.z -= speed_сomponent * sensor_direction.z

                if self.direction.xz.dot(sensor_direction.xz) > 0.8:
                    #self.angle = i
                    did_push = True

                self.position -= self.rotation * (sensor_direction * (15 * scale - intersect.distance))

        if self.is_pushing and not did_push:
            self.angle_override = False

        self.is_pushing = is_pushing = did_push
        if is_pushing:
            self.angle_override = True

        # Check ground collision
        modeMatrix = self.rotation  # .makeModeMatrix()
        modeVector = vec3(0, -1, 0)
        position = self.position

        intersects = [
            physics.ray_test(position + vec3(-9 * scale, 0, -9 * scale), modeVector, 36 * scale),
            physics.ray_test(position + vec3(-9 * scale, 0, 9 * scale), modeVector, 36 * scale),
            physics.ray_test(position + vec3(9 * scale, 0, -9 * scale), modeVector, 36 * scale),
            physics.ray_test(position + vec3(9 * scale, 0, 9 * scale), modeVector, 36 * scale),
        ]

        min_intersect = min(intersects, key=lambda intersect: intersect.distance)

        intersect_normal = vec3(0, 0, 0)
        up_vector = vec3(0, 1, 0)
        count = 0

        for intersect in intersects:
            if math.isfinite(intersect.distance):
                hit_normal = intersect.hit_normal
                if hit_normal.dot(up_vector) > 0.8:
                    intersect_normal += hit_normal
                    count += 1

        if count:
            intersect_normal = (intersect_normal * (1 / count)).normalize()

        # if count != 4:
        #     self.on_ledge = True
        # else:
        #     self.on_ledge = False

        if min_intersect.has_hit and count:
            if (min_intersect.distance <= (20 * scale) or self.on_ground) and self.speed.y <= 0:

                self.position += self.rotation * vec3(0, (20 * scale) - min_intersect.distance, 0)
                if min_intersect.hit_normal.dot(self.rotation * vec3(0, 1, 0)) > 0.0:
                    self.align_player_to_normal(intersect_normal, delta_time)

                if not self.on_ground:
                    self.convert_motion_to_ground()

                if self.state == STATE_JUMPING or self.state == STATE_SPRING:
                    self.state = STATE_NORMAL

                self.speed.y = 0.0

        else:
            self.convert_motion_to_air()
            self.rotation = mat3x3()

        # Check ceiling collision
        modeVector = modeMatrix * vec3(0, 1, 0)
        position = self.position
        intersectA1 = physics.ray_test(position + vec3(-9 * scale, 0, -9 * scale), modeVector, 36 * scale)
        intersectA2 = physics.ray_test(position + vec3(-9 * scale, 0, 9 * scale), modeVector, 36 * scale)
        intersectB1 = physics.ray_test(position + vec3(9 * scale, 0, -9 * scale), modeVector, 36 * scale)
        intersectB2 = physics.ray_test(position + vec3(9 * scale, 0, 9 * scale), modeVector, 36 * scale)

        minIntersectA = intersectA1 if intersectA1.distance < intersectA2.distance else intersectA2
        minIntersectB = intersectB1 if intersectB1.distance < intersectB2.distance else intersectB2
        minIntersect = minIntersectA if minIntersectA.distance < minIntersectB.distance else minIntersectB

        if minIntersect.has_hit:
            if minIntersect.distance <= 20 * scale and self.speed.y >= 0:
                self.position -= self.rotation * vec3(0, 20 * scale - minIntersect.distance, 0)
                self.speed.y = 0.0

    def reposition_player(self, delta_time):
        self.position += (self.rotation * self.speed) * delta_time

    def handle_animations(self, delta_time):
        speed_modulus = self.speed.xz.length()
        state = self.state
        current_animation = self.current_animation

        if self.on_ground:
            if state == STATE_ROLLING:
                current_animation = "spinning"
            elif state == STATE_CROUCH_DOWN:
                current_animation = "crouchDown"
            elif state == STATE_LOOK_UP:
                current_animation = "lookUp"
            elif state == STATE_SPINDASH:
                current_animation = "spinning"
            else:
                if speed_modulus <= SPEED_BASED_ANIMATION_GAP:
                    if self.on_ledge:
                        current_animation = "balancing"
                    else:
                        if self.is_pushing:
                            current_animation = "pushing"
                        else:
                            if self.time_waiting > 5:
                                current_animation = "waiting"
                            else:
                                current_animation = "stopped"

                elif speed_modulus < maxsp - SPEED_BASED_ANIMATION_GAP:
                    if self.is_pushing:
                        current_animation = "pushing"
                    else:
                        current_animation = "walking"

                else:
                    current_animation = "running"

        else:
            if state == STATE_JUMPING:
                current_animation = "spinning"
            elif state == STATE_SPRING:
                current_animation = "spring"
            else:
                if speed_modulus <= SPEED_BASED_ANIMATION_GAP:
                    current_animation = "stopped"
                elif speed_modulus < maxsp - SPEED_BASED_ANIMATION_GAP:
                    current_animation = "walking"
                else:
                    current_animation = "running"

        self.update_animations(delta_time)

        if current_animation == "spinning":
            self.animation_transform = glm.rotate(mat4x4(), -self.time * 40.0, vec3(1, 0, 0))
            if math.floor(self.time * 40) % 2 == 0:
                self.spinball_visible = True
            else:
                self.spinball_visible = False

        elif current_animation == "spring":
            self.animation_transform = glm.rotate(mat4x4(), -self.time * 10.0, 0, 0, 1)
            self.spinball_visible = False
        else:
            self.spinball_visible = False
            self.animation_transform = mat4x4()

        self.current_animation = current_animation

    def update_animations(self, delta_time):
        current_animation = self.current_animation
        if current_animation != self.last_animation:
            self.last_animation = current_animation

            animation = animations.get(current_animation)
            if animation:
                self.animation_player.start_animation(animation)



def frange(x, y, jump):
    while x < y:
        yield x
        x += jump
