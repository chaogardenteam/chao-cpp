import math
import inputengine
from glm import mat3x3, mat4x4, vec3, look_at, perspective, translate
from entity import PyEntity
from engine import Engine
from renderparams import RenderParams

up_vector = vec3(0, 1, 0)
scale = 1 / 40.0


class Camera(PyEntity):

    player = None

    def __init__(self):
        self.position = vec3(0, 0, 0)
        self.center = vec3(0, 0, 0)

        self.target_camera_angle = math.pi / 2
        self.camera_angle = math.pi / 2
        self.camera_distance = (128 + 64) * scale
        self.camera_height = 48 * scale

        self.target_camera_height = 64 * scale
        self.camera_box_position = vec3(0, 0, 0)
        self.camera_box_width = 16 * scale
        self.camera_box_height = 64 * scale
        self.camera_box_y_offset = 32 * scale  # 128 - 96
        self.camera_offset_y = 16 * scale  # 128 - 112

        self.input = Engine().input

    def get_projection_matrix(self):
        return perspective(
            45.0,       # The horizontal Field of View
            4.0 / 3.0,  # Aspect Ratio
            0.1,        # Near clipping plane
            2000.0      # Far clipping plane
        )

    def get_view_matrix(self):
        return look_at(self.position, self.center, up_vector)

    def update(self, delta_time):
        player = self.player
        if player:
            self.update_camera(delta_time, player)
        render_params = RenderParams()
        render_params.set_matrix(b"projMatrix", self.get_projection_matrix())
        render_params.set_matrix(b"viewMatrix", self.get_view_matrix())

        Engine().scene.camera_node.set_render_params(render_params)

    def set_target_player(self, player):
        pass

    def update_camera(self, delta_time, player):
        if self.input.is_key_pressed(inputengine.KEY_CAMERA_LEFT):
            self.target_camera_angle += math.pi / 4
        if self.input.is_key_pressed(inputengine.KEY_CAMERA_RIGHT):
            self.target_camera_angle -= math.pi / 4

        self.camera_angle -= (self.camera_angle - self.target_camera_angle) * 4.0 * delta_time
        self.camera_height -= (self.camera_height -
                               (self.target_camera_height - 0.0 * min(0, player.speed.y))) * 1.0 * delta_time

        transformed_speed = (player.rotation * player.speed) * delta_time

        # Horizontal movement
        camera_box_position = self.camera_box_position
        camera_box_width = self.camera_box_width
        camera_box_height = self.camera_box_height
        camera_box_y_offset = self.camera_box_y_offset

        camera_transition = 16 * (60 * delta_time)
        camera_transition_slow = 6 * (60 * delta_time)

        position = player.position

        # Horizontal movement
        if position.x > (camera_box_position.x + camera_box_width / 2):
            camera_box_position.x += min(
                position.x - (camera_box_position.x + camera_box_width / 2), camera_transition)

        if position.x < (camera_box_position.x - camera_box_width / 2):
            camera_box_position.x += max(
                position.x - (camera_box_position.x - camera_box_width / 2), -camera_transition)

        if position.z > (camera_box_position.z + camera_box_width / 2):
            camera_box_position.z += min(
                position.z - (camera_box_position.z + camera_box_width / 2), camera_transition)

        if position.z < (camera_box_position.z - camera_box_width / 2):
            camera_box_position.z += max(
                position.z - (camera_box_position.z - camera_box_width / 2), -camera_transition)

        # Vertical movement
        if not player.on_ground:
            if position.y > camera_box_position.y + camera_box_height:
                camera_box_position.y += min(
                    position.y - (camera_box_position.y + camera_box_height), camera_transition)

            if position.y < camera_box_position.y:
                camera_box_position.y += max(
                    position.y - (camera_box_position.y), -camera_transition)

        else:
            if position.y > camera_box_position.y + camera_box_y_offset:
                if abs(transformed_speed.y) > 6 * delta_time:
                    # Fast catch up
                    camera_box_position.y += min(
                        position.y - (camera_box_position.y + camera_box_y_offset), camera_transition)
                else:
                    # Slow catch up
                    camera_box_position.y += min(
                        position.y - (camera_box_position.y + camera_box_y_offset), camera_transition_slow)

            if position.y < camera_box_position.y + camera_box_y_offset:
                if abs(transformed_speed.y) > 6 * delta_time:
                    # Fast catch up
                    camera_box_position.y += max(
                        position.y - (camera_box_position.y + camera_box_y_offset), -camera_transition)
                else:
                    # Slow catch up
                    camera_box_position.y += max(
                        position.y - (camera_box_position.y + camera_box_y_offset), -camera_transition_slow)

        self.camera_box_position = camera_box_position

        self.position = camera_box_position + vec3(
            math.cos(self.camera_angle) * self.camera_distance,
            self.camera_height + self.camera_offset_y,
            math.sin(self.camera_angle) * self.camera_distance
        )

        self.center = camera_box_position + vec3(0, self.camera_offset_y, 0)

        rotation = look_at(self.position, self.center, up_vector)
        self.rotation = mat3x3(rotation.inverse())
        if self.player:
            self.player.camera_rotation = self.rotation
