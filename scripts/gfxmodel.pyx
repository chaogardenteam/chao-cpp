from defs.glmesh cimport GLMesh
from defs.resources cimport Resources as CResources
from libcpp.string cimport string
from libcpp_ext.memory cimport shared_ptr

cdef class GfxModel:
    cpdef load_animation(self, bytes filename, bytes animationName, int flags=0):
        self.thisptr.get().loadAnimation(filename, animationName, flags)