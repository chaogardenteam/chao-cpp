cdef class PhysicsEngine:

    cpdef ray_test(self, vec3 ray_from_world, vec3 ray_to_world, max_distance=None):
        if max_distance:
            # We have ray defined with direction and distance instead of end point
            ray_to_world = ray_from_world + (ray_to_world * max_distance)

        result = ClosestRayResult(ray_from_world, ray_to_world)
        self.thisptr.rayTest(ray_from_world.vec, ray_to_world.vec, result.result[0])
        result.check_result()
        return result
