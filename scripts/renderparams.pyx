from defs.renderparams cimport RenderParams as CRenderParams

cdef class RenderParams:

    cpdef set_matrix(self, bytes name, mat4x4 mat):
        self.params.matrices[name] = mat.mat

    cpdef set_bone_transforms(self, AnimationPlayer player):
        self.params.matrixArrays["bones"] = player.thisptr.boneTransforms