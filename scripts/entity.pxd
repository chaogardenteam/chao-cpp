from libcpp.map cimport map
from libcpp.vector cimport vector
from libcpp.string cimport string
from renderparams cimport RenderParams
from defs.renderparams cimport RenderParams as CRenderParams

cdef extern from "Python.h":
    cdef void Py_INCREF(object o)
    cdef void Py_DECREF(object o)


cdef extern from "engine/world/scriptedentity.h":
    ctypedef void (*renderfunc)(void* objptr, CRenderParams)
    ctypedef void (*updatefunc)(void* objptr, float)
    ctypedef void (*destroyfunc)(void* objptr)    

    cdef cppclass ScriptedEntity:
        ScriptedEntity()
        void* objPtr
        renderfunc renderCallback
        updatefunc updateCallback
        destroyfunc destroyCallback
        
cdef public class PyEntity[object PyEntity, type EntityType]:
    cpdef update(self, float delta_time)    
    cpdef render(self, RenderParams render_params)
