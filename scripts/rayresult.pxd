from defs.rayresult cimport ClosestRayResult as CClosestRayResult
from glm cimport vec3
from cpython cimport bool

cdef class ClosestRayResult:
    cdef CClosestRayResult* result
    cdef vec3 ray_from_world
    cdef vec3 ray_to_world
    cdef max_distance

    cdef readonly bool has_hit
    cdef readonly vec3 hit_point
    cdef readonly vec3 hit_normal

    cdef void check_result(self)