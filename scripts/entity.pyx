# distutils: language = c++
import sys
import traceback
from libcpp.map cimport map
from libcpp.vector cimport vector
from libcpp.string cimport string

        
cdef public class PyEntity[object PyEntity, type EntityType]:
    cpdef update(self, float delta_time):
        pass
    
    cpdef render(self, RenderParams render_params):
        pass
            
cdef public ScriptedEntity* create_entity(string name):
    try:
        import entities
    except Exception as e:
        print(traceback.format_exc())

    cdef ScriptedEntity *entity = new ScriptedEntity()
    cdef PyEntity pyentity
    pyname = name.decode('utf-8')

    try:
        pyentity_class = getattr(entities, pyname)
    except AttributeError as e:
        print("Can't get class for {} entity".format(pyname))
    else:
        pyentity = pyentity_class()
        Py_INCREF(pyentity)
        entity.objPtr = <void*>pyentity
        entity.updateCallback = entity_update
        entity.renderCallback = entity_render
        entity.destroyCallback = entity_destroy
    
    return entity
    
cdef public void entity_update(void* objptr, float delta_time):
    entity = <PyEntity>objptr
    entity.update(delta_time)
    
cdef public void entity_render(void* objptr, CRenderParams render_params):
    entity = <PyEntity>objptr
    py_render_params = RenderParams()
    py_render_params.params = render_params
    entity.render(py_render_params)    

cdef public void entity_destroy(void* objptr):
    entity = <object>objptr
    Py_DECREF(entity)
