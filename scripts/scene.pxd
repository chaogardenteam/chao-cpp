from defs.scene cimport Scene as CScene
from scenenode cimport SceneNode


cdef class Scene:
    cdef CScene* thisptr
    cpdef add(self, SceneNode node)