from defs.scenenode cimport SceneNode as CSceneNode
from libcpp_ext.memory cimport shared_ptr
from renderparams cimport RenderParams

cdef class SceneNode:
    cdef CSceneNode* thisptr
    cdef bint is_created
    cpdef set_render_params(self, RenderParams params)