from defs.glmesh cimport GLMesh
from defs.resources cimport Resources as CResources
from libcpp.string cimport string
from libcpp_ext.memory cimport shared_ptr
from gfxmodel cimport GfxModel

cdef class Resources:
    cpdef getModel(self, filename, flags=0):
        cdef shared_ptr[GLMesh] glmesh = CResources.getModel(filename, flags)
        model = GfxModel()
        model.thisptr = glmesh
        return model