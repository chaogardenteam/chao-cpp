from defs.glmesh cimport GLMesh
from libcpp_ext.memory cimport shared_ptr

cdef class GfxModel:
    cdef shared_ptr[GLMesh] thisptr
    cpdef load_animation(self, bytes filename, bytes animationName, int flags=?)