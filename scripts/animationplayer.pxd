from defs.animationplayer cimport AnimationPlayer as CAnimationPlayer
from libcpp.string cimport string

cdef class AnimationPlayer:
    cdef CAnimationPlayer* thisptr
    cpdef start_animation(self, string name)
    cpdef play_animation(self, float delta_time)