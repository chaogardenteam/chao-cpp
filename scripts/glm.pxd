from defs cimport glm

cdef class mat3x3:
    cdef glm.mat3x3 mat
    cpdef inverse(self)
    cpdef align_y_axis(self, vec3 target, float alpha)

cdef class mat4x4:
    cdef glm.mat4x4 mat
    cpdef inverse(self)

cdef class vec:
    pass

cdef class vec2(vec):
    cdef glm.vec2 vec
    cpdef dot(self, vec2 x)
    cpdef length(self)

cdef class vec3(vec):
    cdef glm.vec3 vec
    cpdef dot(self, vec3 x)
    cpdef length(self)    
    cpdef normalize(self)

cdef class vec4(vec):
    cdef glm.vec4 vec

cpdef look_at(vec3 eye, vec3 center, vec3 up)
cpdef perspective(float fovy, float aspect, float z_near, float z_far)
cpdef rotate(mat4x4 m, float angle, vec3 axis)
cpdef scale(mat4x4 m, vec3 v)
cpdef translate(mat4x4 m, vec3 v)
